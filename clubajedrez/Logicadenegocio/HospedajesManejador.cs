﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;

namespace Logicadenegocio
{
   public class HospedajesManejador
    {
        HospedajesAccesoDatos _hospedaje = new HospedajesAccesoDatos();
        ParticipantesAccesoDatos _participantes = new ParticipantesAccesoDatos();
        HotelesAccesoDatos _hoteles = new HotelesAccesoDatos();
        public void Guardar(Hospedajes hospedajes)
        {
            _hospedaje.Guardar(hospedajes);
        }
        public void Eliminar(int ID)
        {
           _hospedaje.Eliminar(ID);
        }
        public List<Hospedajes> GetHospedajes(string filtro)
        {
            var listHospedaje = _hospedaje.GetHospedajes(filtro);
            return listHospedaje;
        }
        public List<Participantes> GetParticipantes(string filtro)
        {
            var listParticipante = _hospedaje.GetParticipantes(filtro);
            return listParticipante;
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listHoteles = _hospedaje.GetHoteles(filtro);
            return listHoteles;
        }
    }
}
