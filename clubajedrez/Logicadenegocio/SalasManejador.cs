﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using AccesoaDatos;
using System.Threading.Tasks;

namespace Logicadenegocio
{
    public class SalasManejador
    {
        SalasAccesoDatos _salasManejador = new SalasAccesoDatos();
        public void Guardar(Salas salas)
        {
            _salasManejador.Guardar(salas);
        }
        public void Eliminar(int ID)
        {
            _salasManejador.Eliminar(ID);
        }
        public List<Salas> GetSalas(string filtro)
        {
            var listSalas = _salasManejador.GetSalas(filtro);
            return listSalas;
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listHoteles = _salasManejador.GetHoteles(filtro);
            return listHoteles;
        }
    }
}
