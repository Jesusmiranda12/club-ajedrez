﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;

namespace Logicadenegocio
{
    public class HotelesManejador
    {
        HotelesAccesoDatos _hotelesManejador = new HotelesAccesoDatos();
        public void Guardar(Hoteles hoteles)
        {
            _hotelesManejador.Guardar(hoteles);
        }
        public void Eliminar(int ID)
        {
            _hotelesManejador.Eliminar(ID);
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listHoteles = _hotelesManejador.GetHoteles(filtro);
            return listHoteles;
        }
    }
}
