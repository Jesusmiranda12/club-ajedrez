﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;

namespace Logicadenegocio
{
    public class MovimentosLogicaNegocio
    {
        MovimientosAccesoDatos _movimientosManejador = new MovimientosAccesoDatos();
        public void Guardar(Movimientos movimientos)
        {
            _movimientosManejador.Guardar(movimientos);
        }
        public void eliminar(int ID)
        {
            _movimientosManejador.Eliminar(ID);
        }
        public List<Movimientos>GetMovimientos(string filtro)
        {
            var listMovimientos = _movimientosManejador.GetMivimientos(filtro);
            return listMovimientos;
        }
        public List<Partidas>GetPartidas()
        {
            var listPartidas = _movimientosManejador.GetPartidas();
            return listPartidas;
        }
    }
}
