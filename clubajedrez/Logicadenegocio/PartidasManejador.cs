﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccesoaDatos;
using Entidades;
using System.Threading.Tasks;

namespace Logicadenegocio
{
    public class PartidasManejador
    {
        PartidasAccesoDatos _partidasManejador = new PartidasAccesoDatos();
        public void Guardar(Partidas partidas)
        {
            _partidasManejador.Guardar(partidas);
        }
        public void Eliminar(int ID)
        {
            _partidasManejador.Eliminar(ID);
        }
        public List<Partidas> GetPartidas(string filtro)
        {
            var listPartidas = _partidasManejador.GetPartidas(filtro);
            return listPartidas;
        }
        public List<Jugadores> GetJugadores(string filtro)
        {
            var listJugadores = _partidasManejador.GetJugadores(filtro);
            return listJugadores;
        }
        public List<Arbitros> GetArbitros(string filtro)
        {
            var listArbitros = _partidasManejador.GetArbitros(filtro);
            return listArbitros;
        }
        public List<Salas> GetSalas(string filtro)
        {
            var listSalas = _partidasManejador.GetSalas(filtro);
            return listSalas;
        }
    }
}
