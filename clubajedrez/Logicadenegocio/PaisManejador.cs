﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;

namespace Logicadenegocio
{
    public class PaisManejador
    {
        PaisAccesoDatos _paisManejador = new PaisAccesoDatos();

        public void Guardar(Pais pais)
        {
            _paisManejador.Guardar(pais);
        }
        public void eliminar(int id)
        {
            _paisManejador.Eliminar(id);
        }

        public List<Pais> GetPais(string filtro)
        {
            var listPais = _paisManejador.GetPaises(filtro);
            return listPais;
        }
        public List<Pais> GetPaisR(string filtro)
        {
            var listPais = _paisManejador.GetPaisesRepresenta(filtro);
            return listPais;
        }
    

    }
}
