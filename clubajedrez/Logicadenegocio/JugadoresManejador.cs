﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccesoaDatos;
using Entidades;
using System.Threading.Tasks;

namespace Logicadenegocio
{
    public class JugadoresManejador
    {
        JugadoresAccesoDatos _jugadoresManejador = new JugadoresAccesoDatos();
        public void Guardar(Jugadores jugadores)
        {
            _jugadoresManejador.Guardar(jugadores);
        }
        public void Actualizar(Jugadores jugadores)
        {
            _jugadoresManejador.Actualizar(jugadores);
        }
        public void Eliminar(int ID)
        {
            _jugadoresManejador.Eliminar(ID);
        }
        public List<Jugadores> GetJugadores(string filtro)
        {
            var listJugadores = _jugadoresManejador.GetJugadores(filtro);
            return listJugadores;
        }
        public List<Participantes> GetParticipantes(string filtro)
        {
            var listParticipantes = _jugadoresManejador.GetParticipantes(filtro);
            return listParticipantes;
        }
    }
}
