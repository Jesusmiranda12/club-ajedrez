﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;

namespace Logicadenegocio
{
    public class ArbitrosManejador
    {
        ArbitrosAccesoDatos _arbitrosManejador = new ArbitrosAccesoDatos();
        public void Guardar(Arbitros arbitros)
        {
            _arbitrosManejador.Guardar(arbitros);
        }
        public void Eliminar(int ID)
        {
            _arbitrosManejador.eliminar(ID);
        }
        public List<Arbitros> GetArbitros(string filtro)
        {
            var listArbitros = _arbitrosManejador.GetArbitros(filtro);
            return listArbitros;
        }

    }
}
