﻿namespace clubajedrez
{
    partial class FrmParticipantes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmParticipantes));
            this.gpbParticipantes = new System.Windows.Forms.GroupBox();
            this.btncancelarFoto = new System.Windows.Forms.Button();
            this.txtlogo = new System.Windows.Forms.TextBox();
            this.btnImagen = new System.Windows.Forms.Button();
            this.pblogo = new System.Windows.Forms.PictureBox();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.txtApellidoMaterno = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtApellidoPaterno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtNumeroAsociado = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbPais = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.dtgParticipantes = new System.Windows.Forms.DataGridView();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LblID = new System.Windows.Forms.Label();
            this.GpoFoto = new System.Windows.Forms.GroupBox();
            this.btncerrarImagen = new System.Windows.Forms.Button();
            this.LbNombrel = new System.Windows.Forms.Label();
            this.PBFotoM = new System.Windows.Forms.PictureBox();
            this.mensaje = new System.Windows.Forms.ToolTip(this.components);
            this.gpbParticipantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pblogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgParticipantes)).BeginInit();
            this.GpoFoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBFotoM)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbParticipantes
            // 
            this.gpbParticipantes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gpbParticipantes.Controls.Add(this.btncancelarFoto);
            this.gpbParticipantes.Controls.Add(this.txtlogo);
            this.gpbParticipantes.Controls.Add(this.btnImagen);
            this.gpbParticipantes.Controls.Add(this.pblogo);
            this.gpbParticipantes.Controls.Add(this.txttelefono);
            this.gpbParticipantes.Controls.Add(this.label8);
            this.gpbParticipantes.Controls.Add(this.txtDireccion);
            this.gpbParticipantes.Controls.Add(this.txtApellidoMaterno);
            this.gpbParticipantes.Controls.Add(this.label6);
            this.gpbParticipantes.Controls.Add(this.txtApellidoPaterno);
            this.gpbParticipantes.Controls.Add(this.label5);
            this.gpbParticipantes.Controls.Add(this.txtNombre);
            this.gpbParticipantes.Controls.Add(this.txtNumeroAsociado);
            this.gpbParticipantes.Controls.Add(this.label7);
            this.gpbParticipantes.Controls.Add(this.cmbPais);
            this.gpbParticipantes.Controls.Add(this.label3);
            this.gpbParticipantes.Controls.Add(this.btnCancelar);
            this.gpbParticipantes.Controls.Add(this.label4);
            this.gpbParticipantes.Controls.Add(this.label2);
            this.gpbParticipantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbParticipantes.ForeColor = System.Drawing.Color.White;
            this.gpbParticipantes.Location = new System.Drawing.Point(2, 212);
            this.gpbParticipantes.Name = "gpbParticipantes";
            this.gpbParticipantes.Size = new System.Drawing.Size(683, 159);
            this.gpbParticipantes.TabIndex = 31;
            this.gpbParticipantes.TabStop = false;
            this.gpbParticipantes.Text = "NUEVOS PARTICIPANTES";
            this.gpbParticipantes.Enter += new System.EventHandler(this.gpbParticipantes_Enter);
            // 
            // btncancelarFoto
            // 
            this.btncancelarFoto.Image = ((System.Drawing.Image)(resources.GetObject("btncancelarFoto.Image")));
            this.btncancelarFoto.Location = new System.Drawing.Point(481, 119);
            this.btncancelarFoto.Name = "btncancelarFoto";
            this.btncancelarFoto.Size = new System.Drawing.Size(33, 34);
            this.btncancelarFoto.TabIndex = 34;
            this.btncancelarFoto.UseVisualStyleBackColor = true;
            this.btncancelarFoto.Click += new System.EventHandler(this.btncancelarFoto_Click);
            // 
            // txtlogo
            // 
            this.txtlogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlogo.Location = new System.Drawing.Point(409, 84);
            this.txtlogo.Name = "txtlogo";
            this.txtlogo.Size = new System.Drawing.Size(142, 22);
            this.txtlogo.TabIndex = 33;
            this.txtlogo.TextChanged += new System.EventHandler(this.txtlogo_TextChanged);
            // 
            // btnImagen
            // 
            this.btnImagen.Image = ((System.Drawing.Image)(resources.GetObject("btnImagen.Image")));
            this.btnImagen.Location = new System.Drawing.Point(520, 119);
            this.btnImagen.Name = "btnImagen";
            this.btnImagen.Size = new System.Drawing.Size(33, 34);
            this.btnImagen.TabIndex = 32;
            this.btnImagen.UseVisualStyleBackColor = true;
            this.btnImagen.Click += new System.EventHandler(this.btnImagen_Click);
            // 
            // pblogo
            // 
            this.pblogo.Location = new System.Drawing.Point(559, 20);
            this.pblogo.Name = "pblogo";
            this.pblogo.Size = new System.Drawing.Size(100, 86);
            this.pblogo.TabIndex = 31;
            this.pblogo.TabStop = false;
            // 
            // txttelefono
            // 
            this.txttelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelefono.Location = new System.Drawing.Point(409, 84);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(142, 22);
            this.txttelefono.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(336, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 16);
            this.label8.TabIndex = 29;
            this.label8.Text = "Telefono";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion.Location = new System.Drawing.Point(411, 50);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(142, 22);
            this.txtDireccion.TabIndex = 28;
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoMaterno.Location = new System.Drawing.Point(155, 114);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.Size = new System.Drawing.Size(142, 22);
            this.txtApellidoMaterno.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(20, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "Apellido materno";
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPaterno.Location = new System.Drawing.Point(155, 84);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.Size = new System.Drawing.Size(142, 22);
            this.txtApellidoPaterno.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(20, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 16);
            this.label5.TabIndex = 24;
            this.label5.Text = "Apellido paterno";
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(155, 50);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(142, 22);
            this.txtNombre.TabIndex = 23;
            // 
            // txtNumeroAsociado
            // 
            this.txtNumeroAsociado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroAsociado.Location = new System.Drawing.Point(155, 20);
            this.txtNumeroAsociado.Name = "txtNumeroAsociado";
            this.txtNumeroAsociado.Size = new System.Drawing.Size(142, 22);
            this.txtNumeroAsociado.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(325, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 16);
            this.label7.TabIndex = 18;
            this.label7.Text = "Direccion";
            // 
            // cmbPais
            // 
            this.cmbPais.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPais.FormattingEnabled = true;
            this.cmbPais.Location = new System.Drawing.Point(411, 18);
            this.cmbPais.Name = "cmbPais";
            this.cmbPais.Size = new System.Drawing.Size(142, 24);
            this.cmbPais.TabIndex = 17;
            this.cmbPais.SelectedIndexChanged += new System.EventHandler(this.cmbPais_SelectedIndexChanged);
            this.cmbPais.Click += new System.EventHandler(this.cmbPais_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(81, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "Nombre";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(559, 113);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 40);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(364, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Pais";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(7, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Numero asociado";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGuardar.BackColor = System.Drawing.Color.Lime;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(567, 153);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(110, 40);
            this.btnGuardar.TabIndex = 30;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminar.BackColor = System.Drawing.Color.Red;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(567, 103);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(110, 40);
            this.btnEliminar.TabIndex = 29;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // dtgParticipantes
            // 
            this.dtgParticipantes.AllowUserToOrderColumns = true;
            this.dtgParticipantes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dtgParticipantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgParticipantes.Location = new System.Drawing.Point(14, 47);
            this.dtgParticipantes.Name = "dtgParticipantes";
            this.dtgParticipantes.Size = new System.Drawing.Size(547, 146);
            this.dtgParticipantes.TabIndex = 26;
            this.dtgParticipantes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgParticipantes_CellContentClick);
            this.dtgParticipantes.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgParticipantes_CellContentDoubleClick);
            this.dtgParticipantes.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dtgParticipantes_CellMouseClick);
            this.dtgParticipantes.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dtgParticipantes_CellMouseDoubleClick);
            this.dtgParticipantes.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgParticipantes_CellMouseEnter);
            this.dtgParticipantes.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgParticipantes_CellMouseLeave);
            this.dtgParticipantes.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dtgParticipantes_CellToolTipTextNeeded);
            this.dtgParticipantes.DoubleClick += new System.EventHandler(this.dtgParticipantes_DoubleClick);
            this.dtgParticipantes.MouseHover += new System.EventHandler(this.dtgParticipantes_MouseHover);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtBuscar.Location = new System.Drawing.Point(174, 8);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(387, 20);
            this.txtBuscar.TabIndex = 28;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // btnInsertar
            // 
            this.btnInsertar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInsertar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnInsertar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInsertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertar.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertar.Image")));
            this.btnInsertar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInsertar.Location = new System.Drawing.Point(567, 47);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(110, 40);
            this.btnInsertar.TabIndex = 25;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInsertar.UseVisualStyleBackColor = false;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(13, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 18);
            this.label1.TabIndex = 27;
            this.label1.Text = "Buscar Parcipantes";
            // 
            // LblID
            // 
            this.LblID.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LblID.AutoSize = true;
            this.LblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblID.ForeColor = System.Drawing.Color.White;
            this.LblID.Location = new System.Drawing.Point(567, 10);
            this.LblID.Name = "LblID";
            this.LblID.Size = new System.Drawing.Size(17, 18);
            this.LblID.TabIndex = 32;
            this.LblID.Text = "0";
            this.LblID.Visible = false;
            // 
            // GpoFoto
            // 
            this.GpoFoto.Controls.Add(this.btncerrarImagen);
            this.GpoFoto.Controls.Add(this.LbNombrel);
            this.GpoFoto.Controls.Add(this.PBFotoM);
            this.GpoFoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GpoFoto.ForeColor = System.Drawing.Color.White;
            this.GpoFoto.Location = new System.Drawing.Point(190, 32);
            this.GpoFoto.Name = "GpoFoto";
            this.GpoFoto.Size = new System.Drawing.Size(276, 218);
            this.GpoFoto.TabIndex = 33;
            this.GpoFoto.TabStop = false;
            this.GpoFoto.Text = "     Foto del participante";
            // 
            // btncerrarImagen
            // 
            this.btncerrarImagen.Image = ((System.Drawing.Image)(resources.GetObject("btncerrarImagen.Image")));
            this.btncerrarImagen.Location = new System.Drawing.Point(253, 2);
            this.btncerrarImagen.Name = "btncerrarImagen";
            this.btncerrarImagen.Size = new System.Drawing.Size(23, 23);
            this.btncerrarImagen.TabIndex = 35;
            this.btncerrarImagen.UseVisualStyleBackColor = true;
            this.btncerrarImagen.Click += new System.EventHandler(this.btncerrarImagen_Click);
            // 
            // LbNombrel
            // 
            this.LbNombrel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LbNombrel.AutoSize = true;
            this.LbNombrel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNombrel.Location = new System.Drawing.Point(17, 36);
            this.LbNombrel.Name = "LbNombrel";
            this.LbNombrel.Size = new System.Drawing.Size(0, 18);
            this.LbNombrel.TabIndex = 34;
            // 
            // PBFotoM
            // 
            this.PBFotoM.Location = new System.Drawing.Point(55, 71);
            this.PBFotoM.Name = "PBFotoM";
            this.PBFotoM.Size = new System.Drawing.Size(163, 141);
            this.PBFotoM.TabIndex = 36;
            this.PBFotoM.TabStop = false;
            // 
            // mensaje
            // 
            this.mensaje.AutoPopDelay = 10000;
            this.mensaje.InitialDelay = 100;
            this.mensaje.IsBalloon = true;
            this.mensaje.ReshowDelay = 100;
            this.mensaje.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.mensaje.ToolTipTitle = "Informacion";
            // 
            // FrmParticipantes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.ClientSize = new System.Drawing.Size(697, 383);
            this.ControlBox = false;
            this.Controls.Add(this.GpoFoto);
            this.Controls.Add(this.LblID);
            this.Controls.Add(this.gpbParticipantes);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.dtgParticipantes);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.btnInsertar);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmParticipantes";
            this.Text = "Participantes";
            this.Load += new System.EventHandler(this.Participantes_Load);
            this.Click += new System.EventHandler(this.FrmParticipantes_Click);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FrmParticipantes_MouseClick);
            this.gpbParticipantes.ResumeLayout(false);
            this.gpbParticipantes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pblogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgParticipantes)).EndInit();
            this.GpoFoto.ResumeLayout(false);
            this.GpoFoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBFotoM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbParticipantes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbPais;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.DataGridView dtgParticipantes;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.TextBox txtApellidoMaterno;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtApellidoPaterno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtNumeroAsociado;
        private System.Windows.Forms.Button btnImagen;
        private System.Windows.Forms.PictureBox pblogo;
        private System.Windows.Forms.Label LblID;
        private System.Windows.Forms.TextBox txtlogo;
        private System.Windows.Forms.Button btncancelarFoto;
        private System.Windows.Forms.GroupBox GpoFoto;
        private System.Windows.Forms.PictureBox PBFotoM;
        private System.Windows.Forms.Label LbNombrel;
        private System.Windows.Forms.ToolTip mensaje;
        private System.Windows.Forms.Button btncerrarImagen;
    }
}