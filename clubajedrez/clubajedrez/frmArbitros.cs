﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Entidades;
using Logicadenegocio;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clubajedrez
{
    public partial class frmArbitros : Form
    {
        private ArbitrosManejador _arbitrosManejador;
        private JugadoresManejador _jugadoresManejador;
        private string ruta;
        private Image nothing = null;
        public frmArbitros()
        {
            InitializeComponent();
            _arbitrosManejador = new ArbitrosManejador();
            _jugadoresManejador = new JugadoresManejador();
        }
        private void limpiar()
        {
            lblId.Text = 0.ToString();
            cmbArbitros.Text = "";
            pbArbitro .Image = nothing;
        }
        private void ActivarBotones(bool insertar, bool eliminar, bool guardar)
        {
            btnInsertar.Enabled = insertar;
            btnEliminar.Enabled = eliminar;
            btnGuardar.Enabled = guardar;
        }
        private void eliminar()
        {
            var id = dtgArbitros.CurrentRow.Cells["Idarbitro"].Value;
            _arbitrosManejador.Eliminar(Convert.ToInt32(id));
        }
        private void Guardar()
        {
            try
            {
                _arbitrosManejador.Guardar(new Arbitros
                {
                    IdArbitro = Convert.ToInt32(cmbArbitros.SelectedValue)
                });
            }
            catch (Exception)
            {
            }        
        }
        private void buscar(string filtro)
        {
            dtgArbitros.DataSource = _arbitrosManejador.GetArbitros(filtro);
        }
        private void modificar()
        {
            cmbArbitros.Text = dtgArbitros.CurrentRow.Cells["Numero_asociado"].Value.ToString();
            txtFoto.Text = dtgArbitros.CurrentRow.Cells["Foto"].Value.ToString();
            var direccion = ruta + txtFoto.Text;
            pbArbitro.ImageLocation = direccion;
            pbArbitro.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        private void ArbitrosCombo(string filtro)
        {
            cmbArbitros.DataSource = _jugadoresManejador.GetParticipantes(filtro);
            cmbArbitros.ValueMember = "idparticipante";
            cmbArbitros.DisplayMember = "numeroasociado";
        }
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ActivarBotones(false, false, true);
            cmbArbitros.Enabled = true;
            gpbArbitros.Visible = true;
        }
        private void frmArbitros_Load(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            ruta = Application.StartupPath + "\\ PARTICIPANTES \\";
            gpbArbitros.Visible = false;
            ArbitrosCombo("");
            buscar("");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {         
            Guardar();
            buscar("");
            limpiar();
            ActivarBotones(true, true, false);
            gpbArbitros.Visible = false;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void dtgArbitros_DoubleClick(object sender, EventArgs e)
        {
            gpbArbitros.Visible = true;
            ActivarBotones(false, false, false);
            cmbArbitros.Enabled = false;
            try
            {
                modificar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            ActivarBotones(true, true, false);
            gpbArbitros.Visible = false;
        }

        private void txtBuscararbitro_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscararbitro.Text);
        }

    }
}
