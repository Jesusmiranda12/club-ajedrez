﻿namespace clubajedrez
{
    partial class frmArbitros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmArbitros));
            this.pbArbitro = new System.Windows.Forms.PictureBox();
            this.gpbArbitros = new System.Windows.Forms.GroupBox();
            this.cmbArbitros = new System.Windows.Forms.ComboBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscararbitro = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.lblId = new System.Windows.Forms.Label();
            this.txtFoto = new System.Windows.Forms.TextBox();
            this.dtgArbitros = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pbArbitro)).BeginInit();
            this.gpbArbitros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgArbitros)).BeginInit();
            this.SuspendLayout();
            // 
            // pbArbitro
            // 
            this.pbArbitro.Location = new System.Drawing.Point(494, 43);
            this.pbArbitro.Margin = new System.Windows.Forms.Padding(2);
            this.pbArbitro.Name = "pbArbitro";
            this.pbArbitro.Size = new System.Drawing.Size(110, 100);
            this.pbArbitro.TabIndex = 40;
            this.pbArbitro.TabStop = false;
            // 
            // gpbArbitros
            // 
            this.gpbArbitros.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gpbArbitros.Controls.Add(this.cmbArbitros);
            this.gpbArbitros.Controls.Add(this.btnCancelar);
            this.gpbArbitros.Controls.Add(this.pbArbitro);
            this.gpbArbitros.Controls.Add(this.label2);
            this.gpbArbitros.Controls.Add(this.label3);
            this.gpbArbitros.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbArbitros.ForeColor = System.Drawing.Color.White;
            this.gpbArbitros.Location = new System.Drawing.Point(10, 201);
            this.gpbArbitros.Name = "gpbArbitros";
            this.gpbArbitros.Size = new System.Drawing.Size(609, 151);
            this.gpbArbitros.TabIndex = 39;
            this.gpbArbitros.TabStop = false;
            this.gpbArbitros.Text = "Nuevo Arbitro";
            // 
            // cmbArbitros
            // 
            this.cmbArbitros.FormattingEnabled = true;
            this.cmbArbitros.Location = new System.Drawing.Point(185, 53);
            this.cmbArbitros.Margin = new System.Windows.Forms.Padding(2);
            this.cmbArbitros.Name = "cmbArbitros";
            this.cmbArbitros.Size = new System.Drawing.Size(114, 26);
            this.cmbArbitros.TabIndex = 10;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(336, 44);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 40);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Numero Asociado";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(478, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 18);
            this.label3.TabIndex = 35;
            this.label3.Text = "Foto del Arbitro";
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminar.BackColor = System.Drawing.Color.Red;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(509, 109);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(110, 40);
            this.btnEliminar.TabIndex = 38;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnInsertar
            // 
            this.btnInsertar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInsertar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnInsertar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInsertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertar.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertar.Image")));
            this.btnInsertar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInsertar.Location = new System.Drawing.Point(509, 62);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(110, 40);
            this.btnInsertar.TabIndex = 37;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInsertar.UseVisualStyleBackColor = false;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(79, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 18);
            this.label1.TabIndex = 36;
            this.label1.Text = "Buscar Arbitro";
            // 
            // txtBuscararbitro
            // 
            this.txtBuscararbitro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtBuscararbitro.Location = new System.Drawing.Point(209, 31);
            this.txtBuscararbitro.Name = "txtBuscararbitro";
            this.txtBuscararbitro.Size = new System.Drawing.Size(264, 20);
            this.txtBuscararbitro.TabIndex = 33;
            this.txtBuscararbitro.TextChanged += new System.EventHandler(this.txtBuscararbitro_TextChanged);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGuardar.BackColor = System.Drawing.Color.Lime;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(509, 155);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(110, 40);
            this.btnGuardar.TabIndex = 43;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lblId
            // 
            this.lblId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.ForeColor = System.Drawing.Color.White;
            this.lblId.Location = new System.Drawing.Point(479, 33);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(17, 18);
            this.lblId.TabIndex = 44;
            this.lblId.Text = "0";
            this.lblId.Visible = false;
            // 
            // txtFoto
            // 
            this.txtFoto.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtFoto.Location = new System.Drawing.Point(249, 175);
            this.txtFoto.Name = "txtFoto";
            this.txtFoto.Size = new System.Drawing.Size(224, 20);
            this.txtFoto.TabIndex = 45;
            // 
            // dtgArbitros
            // 
            this.dtgArbitros.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dtgArbitros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgArbitros.Location = new System.Drawing.Point(16, 62);
            this.dtgArbitros.Name = "dtgArbitros";
            this.dtgArbitros.Size = new System.Drawing.Size(463, 133);
            this.dtgArbitros.TabIndex = 46;
            this.dtgArbitros.DoubleClick += new System.EventHandler(this.dtgArbitros_DoubleClick);
            // 
            // frmArbitros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.ClientSize = new System.Drawing.Size(656, 355);
            this.ControlBox = false;
            this.Controls.Add(this.dtgArbitros);
            this.Controls.Add(this.txtFoto);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.gpbArbitros);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnInsertar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBuscararbitro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmArbitros";
            this.Load += new System.EventHandler(this.frmArbitros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbArbitro)).EndInit();
            this.gpbArbitros.ResumeLayout(false);
            this.gpbArbitros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgArbitros)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbArbitro;
        private System.Windows.Forms.GroupBox gpbArbitros;
        private System.Windows.Forms.ComboBox cmbArbitros;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscararbitro;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtFoto;
        private System.Windows.Forms.DataGridView dtgArbitros;
    }
}