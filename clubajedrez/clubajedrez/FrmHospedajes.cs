﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Logicadenegocio;

namespace clubajedrez
{
    public partial class FrmHospedajes : Form
    {
        HospedajesManejador _hospedajesManejador;
        public FrmHospedajes()
        {
            InitializeComponent();
            _hospedajesManejador = new HospedajesManejador();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void FrmHospedajes_Load(object sender, EventArgs e)
        {
            buscar(" ");
            buscar("");
            comboHotel(" ");
            comboHotel("");
            comboParticipantes(" ");
            comboParticipantes("");
            activarBotones(false, true, true, false);
            mostarGrupo(false);
        }
        private void Guardar()
        {
            _hospedajesManejador.Guardar(new Hospedajes {
               IdHospedaje=Convert.ToInt32(LblId.Text),
                NumeroAsociado=cmbParticipante.SelectedValue.ToString(),
                Nombrehotel=cmbHotel.SelectedValue.ToString(),
                FechaEntrada=dtpnE.Text,
                FechaSalida=dpnS.Text

            });
        }
        private void comboParticipantes(string filtro)
        {
            cmbParticipante.DataSource = _hospedajesManejador.GetParticipantes(filtro);
            cmbParticipante.ValueMember = "IdParticipante";
            cmbParticipante.DisplayMember = "NumeroAsociado";     
        }
        private void comboHotel(string filtro)
        {
            cmbHotel.DataSource = _hospedajesManejador.GetHoteles(filtro);
           cmbHotel.ValueMember = "IdHotel";
            cmbHotel.DisplayMember = "Nombreh";          
        }
        private void activarBotones(bool guardar, bool eliminar,bool insertar,bool cancelar )
        {
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnInsertar.Enabled = insertar;
            btnCancelar.Enabled = cancelar;
        }
        private void mostarGrupo(bool mostrar)
        {
            gpbHospedajes.Visible = mostrar;
        }
        private void buscar(string filtro)
        {
            dtgHospedaje.DataSource = _hospedajesManejador.GetHospedajes(filtro);
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void cmbParticipante_Click(object sender, EventArgs e)
        {
            comboParticipantes(" ");
            comboParticipantes("");
        }
        private void cmbHotel_Click(object sender, EventArgs e)
        {
            comboHotel(" ");
            comboHotel("");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
            buscar(" ");
            buscar("");
            activarBotones(false, true, true, false);
            mostarGrupo(false);
            limpiar();
        }
        private void eliminarRegistro()
        {
            var IdHospedaje = dtgHospedaje.CurrentRow.Cells["idhospedaje"].Value;
            _hospedajesManejador.Eliminar(Convert.ToInt32(IdHospedaje));
        }
        private void modificar()
        {
            LblId.Text = dtgHospedaje.CurrentRow.Cells["idhospedaje"].Value.ToString();
            cmbParticipante.Text = dtgHospedaje.CurrentRow.Cells["NumeroAsociado"].Value.ToString();
            cmbHotel.Text = dtgHospedaje.CurrentRow.Cells["Nombrehotel"].Value.ToString();
            dtpnE.Text = dtgHospedaje.CurrentRow.Cells["fechaentrada"].Value.ToString();
            dpnS.Text = dtgHospedaje.CurrentRow.Cells["fechasalida"].Value.ToString();
        }
        private void limpiar()
        {
            LblId.Text = 0.ToString();
        }
        private void dtpnE_ValueChanged(object sender, EventArgs e)
        {

        }
        private void gpbHospedajes_Enter(object sender, EventArgs e)
        {

        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            mostarGrupo(true);
            activarBotones(true, false, false, true);
        }

        private void dtgHospedaje_DoubleClick(object sender, EventArgs e)
        {
            mostarGrupo(true);
            activarBotones(true, false, false, true);
            modificar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminarRegistro();
                    buscar("");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            activarBotones(false, true, true, false);
            mostarGrupo(false);
        }
    }
}
