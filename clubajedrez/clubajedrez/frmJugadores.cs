﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Entidades;
using Logicadenegocio;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace clubajedrez
{
    public partial class frmJugadores : Form
    {
        private string ruta;
        private JugadoresManejador _jugadoresManejador;
        private Image Nothing = null;
        public frmJugadores()
        {
            InitializeComponent();
            _jugadoresManejador = new JugadoresManejador();
        }
        private void limpiar()
        {
            lblId.Text = 0.ToString();
            txtniveldejuego.Text = "";
            cmbJugador.Text = "";
            pbJugador.Image = Nothing;
        }
        private void ActivarBotones(bool insertar, bool eliminar, bool guardar, bool actualizar, bool cancelar)
        {
            btnInsertar.Enabled = insertar;
            btnEliminar.Enabled = eliminar;
            btnGuardar.Enabled = guardar;
            btnActualizar.Enabled = actualizar;
            btnCancelar.Enabled = cancelar;
        }
        private void eliminar()
        {
            var id = dtgJugadores.CurrentRow.Cells["Idjugador"].Value;
            _jugadoresManejador.Eliminar(Convert.ToInt32(id));

        }
        private void Guardar()
        {
            try
            {
                _jugadoresManejador.Guardar(new Jugadores
                {
                    Idjugador = Convert.ToInt32(cmbJugador.SelectedValue),
                    Nivel = Convert.ToInt32(txtniveldejuego.Text)
                });
            }
            catch (Exception)
            {
            }         
        }
        private void buscar(string filtro)
        {
            dtgJugadores.DataSource = _jugadoresManejador.GetJugadores(filtro);
        }
        private void modificar()
        {
            cmbJugador.Text = dtgJugadores.CurrentRow.Cells["numeroasociado"].Value.ToString();
            txtniveldejuego.Text = dtgJugadores.CurrentRow.Cells["nivel"].Value.ToString();
            txtFoto.Text = dtgJugadores.CurrentRow.Cells["Foto"].Value.ToString();
            var direccion = ruta + txtFoto.Text;
            pbJugador.ImageLocation = direccion;
            pbJugador.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ActivarBotones(false, false, true, false, true);
            cmbJugador.Enabled = true;
            gpbJugadores.Visible = true;
        }

        private void frmJugadores_Load(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false, false, false);
            ruta = Application.StartupPath + "\\ PARTICIPANTES \\";
            gpbJugadores.Visible = false;
            jugadoresCombo("");
            buscar("");
        }
        private void jugadoresCombo(string filtro)
        {
            cmbJugador.DataSource = _jugadoresManejador.GetParticipantes(filtro);
            cmbJugador.ValueMember = "idparticipante";
            cmbJugador.DisplayMember = "numeroasociado";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {        
            Guardar();
            buscar("");                      
            limpiar();
            ActivarBotones(true, true, false, false, false);
            gpbJugadores.Visible = false;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void dtgJugadores_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void dtgJugadores_DoubleClick_1(object sender, EventArgs e)
        {
            gpbJugadores.Visible = true;
            ActivarBotones(false,false,false,true, true);
            cmbJugador.Enabled = false;
            try
            {
                modificar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            ActivarBotones(true,true,false,false,false);
            gpbJugadores.Visible = false;
        }

        private void txtBuscarjugador_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscarjugador.Text);
        }
        private void Actualizar()
        {
            _jugadoresManejador.Actualizar(new Jugadores
            {
                Idjugador = Convert.ToInt32(cmbJugador.SelectedValue),
                Nivel = Convert.ToInt32(txtniveldejuego.Text)
            });
        }
        private void btnActualizar_Click(object sender, EventArgs e)
        {
            Actualizar();
            buscar("");
            ActivarBotones(true, true, false, false, false);
        }
    }
}
