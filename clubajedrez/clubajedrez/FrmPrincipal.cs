﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace clubajedrez
{
    public partial class FrmPrincipal : Form
    {
         private Timer ti;
        private Form formularioactivo = null;
        public FrmPrincipal()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventoTimer);
            InitializeComponent();
            ti.Enabled = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        private void eventoTimer(object ob, EventArgs evt)
        {
            DateTime hoy = DateTime.Now;
            lblReloj.Text = hoy.ToString("hh:mm:ss tt");
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnmaximizar.Visible = false;
            btnRestaurar.Visible = true;    

        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnRestaurar.Visible = false;
            btnmaximizar.Visible = true;
        }

        private void btnArbitros_Click(object sender, EventArgs e)
        {

        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            MenuVertical.Width = 63;

            PanelContenedor.Width = 808;

        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        private void abrir(Form abrir)
        {
            if (formularioactivo != null)
            {
                formularioactivo.Close();
            }
            formularioactivo = abrir;
            abrir.TopLevel = false;
            abrir.FormBorderStyle = FormBorderStyle.None;
            abrir.Dock = DockStyle.Fill;
            PanelContenedor.Controls.Add(abrir);
            PanelContenedor.Tag = abrir;
            abrir.BringToFront();
            abrir.Show();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 200)
            {
                MenuVertical.Width = 63;
               /* this.mensaje.SetToolTip(btnParticipantes, "Ventana participantes");
                this.mensaje.SetToolTip(btnJugadores, "Ventana jugadores");
                this.mensaje.SetToolTip(btnArbitros, "Ventana arbitros");
                this.mensaje.SetToolTip(btnPaises, "Ventana paises");
                this.mensaje.SetToolTip(btnHoteles, "Ventana hoteles");
                this.mensaje.SetToolTip(btnSalas, "Ventana salas");
                this.mensaje.SetToolTip(btnHospedajes, "Ventana hospedajes");
                this.mensaje.SetToolTip(btnPartidas, "Ventana partidas");
                this.mensaje.SetToolTip(btnMovimientos, "Ventana movimientos");*/
                //this.mensaje.Active = true;
            }
            else
            {
                MenuVertical.Width = 200;
                //this.mensaje.Active = false;
            }
        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
