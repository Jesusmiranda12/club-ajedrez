﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Logicadenegocio;

namespace clubajedrez
{
    public partial class FrmMovimientos : Form
    {
        MovimentosLogicaNegocio _movimentosLogicaNegocio;
        public FrmMovimientos()
        {
            InitializeComponent();
            _movimentosLogicaNegocio = new MovimentosLogicaNegocio();
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            mostrarGrupoMovimientos(false);
            activarBotones(false, true, true, false);
            buscar(" ");
            buscar("");
            comboPartida();
        }
        private void Guardar()
        {
            _movimentosLogicaNegocio.Guardar(new Movimientos {
                IdMovimiento = Convert.ToInt32(lblID.Text),
                NumeroMovimiento = Convert.ToInt32(txtNumeroMovimiento.Text),
                Posiciones = txtPosicion.Text,
                Partida = cmbPartida.SelectedValue.ToString(),
                Comentario = txtComentario.Text
                
            });
        }
        private void eliminar()
        {
            var Id = dtgMovimientos.CurrentRow.Cells["IdMovimiento"].Value.ToString();
            _movimentosLogicaNegocio.eliminar(Convert.ToInt32(Id));
        }
        private void modificar()
        {
            lblID.Text = dtgMovimientos.CurrentRow.Cells["IdMovimiento"].Value.ToString();
            txtNumeroMovimiento.Text = dtgMovimientos.CurrentRow.Cells["NumeroMovimiento"].Value.ToString();
            txtPosicion.Text = dtgMovimientos.CurrentRow.Cells["Posiciones"].Value.ToString();
            cmbPartida.Text = dtgMovimientos.CurrentRow.Cells["Partida"].Value.ToString();
            txtComentario.Text = dtgMovimientos.CurrentRow.Cells["Comentario"].Value.ToString();
        }
        private void comboPartida()
        {
            cmbPartida.DataSource = _movimentosLogicaNegocio.GetPartidas();
            cmbPartida.ValueMember = "idpartida";
            cmbPartida.DisplayMember = "idpartida";
        }
        private void cmbPartida_Click(object sender, EventArgs e)
        {
            comboPartida();
        }
        private void limpiar()
        {
            lblID.Text = 0.ToString();
            txtNumeroMovimiento.Text = "";
            txtPosicion.Text = "";
            txtComentario.Text = "";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
            limpiar();
            mostrarGrupoMovimientos(false);
            activarBotones(false, true, true, false);
            buscar(" ");
            buscar("");
        }
        private void buscar(string filtro)
        {
            dtgMovimientos.DataSource = _movimentosLogicaNegocio.GetMovimientos(filtro);
        }
        private void activarBotones(bool guardar, bool eliminar, bool insertar, bool cancelar)
        {
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnInsertar.Enabled = insertar;
            btnCancelar.Enabled = cancelar;
        }
        private void mostrarGrupoMovimientos(bool mostrar)
        {
            gpbMovimientos.Visible = mostrar;
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            activarBotones(true, false, false, true);
            mostrarGrupoMovimientos(true);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            mostrarGrupoMovimientos(false);
            activarBotones(false, true, true, false);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminar();
                    buscar(" ");
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void dtgMovimientos_DoubleClick(object sender, EventArgs e)
        {
            modificar();
            mostrarGrupoMovimientos(true);
            activarBotones(true, false, false, true);
        }
    }
}
