﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace clubajedrez
{
    public partial class Form1 : Form
    {
        private Timer ti;
        private Form formularioactivo = null;
        public Form1()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventoTimer);
            InitializeComponent();
            ti.Enabled = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        private void eventoTimer(object ob, EventArgs evt)
        {
            DateTime hoy = DateTime.Now;
            lblReloj.Text = hoy.ToString("hh:mm:ss tt");
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
           
        }

        private void button10_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MenuVertical.Width = 63;
            if (MenuVertical.Width <= 63)
            {
                this.mensaje.SetToolTip(btnParticipantes, "Ventana participantes");
                this.mensaje.SetToolTip(btnJugadores, "Ventana jugadores");
                this.mensaje.SetToolTip(btnArbitros, "Ventana arbitros");
                this.mensaje.SetToolTip(btnPaises, "Ventana paises");
                this.mensaje.SetToolTip(btnHoteles, "Ventana hoteles");
                this.mensaje.SetToolTip(btnSalas, "Ventana salas");
                this.mensaje.SetToolTip(btnHospedajes, "Ventana hospedajes");
                this.mensaje.SetToolTip(btnPartidas, "Ventana partidas");
                this.mensaje.SetToolTip(btnMovimientos, "Ventana movimientos");
            }   
            MenuContenido.Width = 808;
            //MenuVertical.Visible = true;
        }
        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 200)
            {
                MenuVertical.Width = 63;
                this.mensaje.SetToolTip(btnParticipantes, "Ventana participantes");
                this.mensaje.SetToolTip(btnJugadores, "Ventana jugadores");
                this.mensaje.SetToolTip(btnArbitros, "Ventana arbitros");
                this.mensaje.SetToolTip(btnPaises, "Ventana paises");
                this.mensaje.SetToolTip(btnHoteles, "Ventana hoteles");
                this.mensaje.SetToolTip(btnSalas, "Ventana salas");
                this.mensaje.SetToolTip(btnHospedajes, "Ventana hospedajes");
                this.mensaje.SetToolTip(btnPartidas, "Ventana partidas");
                this.mensaje.SetToolTip(btnMovimientos, "Ventana movimientos");
                this.mensaje.Active = true;
            }
            else
            {
                MenuVertical.Width = 200;
                this.mensaje.Active = false;
            }
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

      
        private void abrir(Form abrir)
        {
            if (formularioactivo != null)
            {
                formularioactivo.Close();
            }
            formularioactivo = abrir;
            abrir.TopLevel = false;
            abrir.FormBorderStyle = FormBorderStyle.None;
            abrir.Dock = DockStyle.Fill;
            MenuContenido.Controls.Add(abrir);
            MenuContenido.Tag = abrir;
            abrir.BringToFront();
            abrir.Show();
        }
  
        private void MenuContenido_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMaximizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

      

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnRestaurar.Visible = false;
            btnMaximizar.Visible = true;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnParticipantes_Click_1(object sender, EventArgs e)
        {
            abrir(new FrmParticipantes());
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnJugadores_Click_1(object sender, EventArgs e)
        {
            abrir(new frmJugadores());
        }

        private void btnArbitros_Click_1(object sender, EventArgs e)
        {
            abrir(new frmArbitros());
        }

        private void btnPaises_Click_1(object sender, EventArgs e)
        {
            abrir(new frmPaises());

        }

        private void btnHoteles_Click_1(object sender, EventArgs e)
        {
            abrir(new frmHoteles());
        }

        private void btnSalas_Click_1(object sender, EventArgs e)
        {
            abrir(new frmSalas());
        }

        private void btnHospedajes_Click_1(object sender, EventArgs e)
        {
            abrir(new FrmHospedajes());
        }

        private void btnPartidas_Click_1(object sender, EventArgs e)
        {
            abrir(new frmPartidas());
        }

        private void btnMovimientos_Click_1(object sender, EventArgs e)
        {
            abrir(new FrmMovimientos());
        }
    }
}
