﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Logicadenegocio;
using Entidades;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clubajedrez
{
    public partial class frmPartidas : Form
    {
        private SalasManejador _salasManejador;
        private PartidasManejador _partidasmanejador;
        public frmPartidas()
        {
            InitializeComponent();
            _partidasmanejador = new PartidasManejador();
            _salasManejador = new SalasManejador();
        }
        private void limpiar()
        {
            lblId.Text = 0.ToString();
            cmbArbitros.Text = "";
            cmbBlancas.Text = "";
            cmbNegras.Text = "";
            cmbSalas.Text = "";
            dtpFecha.Text = "";
        }
        private void ActivarBotones(bool insertar, bool eliminar, bool guardar)
        {
            btnInsertar.Enabled = insertar;
            btnEliminar.Enabled = eliminar;
            btnGuardar.Enabled = guardar;
        }
        private void eliminar()
        {
            var id = dtgPartidas.CurrentRow.Cells["idpartida"].Value;
            _partidasmanejador.Eliminar(Convert.ToInt32(id));
        }
        private void Guardar()
        {
            _partidasmanejador.Guardar(new Partidas
            {
                Idpartida = Convert.ToInt32(lblId.Text),
                JugadorBlancas = cmbBlancas.Text,
                JugadorNegras = cmbNegras.Text,
                Arbitro = cmbArbitros.Text,
                Sala = Convert.ToInt32(cmbSalas.SelectedValue.ToString()),
                Fecha = dtpFecha.Text,
            });

        }
        private void buscar(string filtro)
        {
            dtgPartidas.DataSource = _partidasmanejador.GetPartidas(filtro);
        }
        private void modificar()
        {
            lblId.Text = dtgPartidas.CurrentRow.Cells["idpartida"].Value.ToString();
            cmbBlancas.Text = dtgPartidas.CurrentRow.Cells["jugadorblancas"].Value.ToString();
            cmbNegras.Text = dtgPartidas.CurrentRow.Cells["jugadornegras"].Value.ToString();
            cmbArbitros.Text = dtgPartidas.CurrentRow.Cells["arbitro"].Value.ToString();
            cmbSalas.Text = dtgPartidas.CurrentRow.Cells["sala"].Value.ToString();
            dtpFecha.Text = dtgPartidas.CurrentRow.Cells["fecha"].Value.ToString();
        }
        private void ValoresCombo(string filtro)
        {
            cmbNegras.DataSource = _partidasmanejador.GetJugadores(filtro);
            cmbNegras.ValueMember = "idjugador";
            cmbNegras.DisplayMember = "numeroasociado";
            cmbBlancas.DataSource = _partidasmanejador.GetJugadores(filtro);
            cmbBlancas.ValueMember = "idjugador";
            cmbBlancas.DisplayMember = "numeroasociado";
            cmbArbitros.DataSource = _partidasmanejador.GetArbitros(filtro);
            cmbArbitros.ValueMember = "idarbitro";
            cmbArbitros.DisplayMember = "numero_asociado";
            cmbSalas.DataSource = _partidasmanejador.GetSalas(filtro);
            cmbSalas.ValueMember = "Idsala";
            cmbSalas.DisplayMember = "numerosala";
        }
        private void frmPartidas_Load(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            gpbPartidas.Visible = false;
            ValoresCombo("");
            buscar("");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            Guardar();
            buscar("");
            gpbPartidas.Visible = false;
            limpiar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ActivarBotones(false, false, true);
            gpbPartidas.Visible = true;
            cmbBlancas.Enabled = true;
        }

        private void dtgPartidas_DoubleClick(object sender, EventArgs e)
        {
            ActivarBotones(false, false, true);
            gpbPartidas.Visible = true;
            try
            {
                modificar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            limpiar();
            gpbPartidas.Visible = false;
        }

        private void cmbSalas_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cmbHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
