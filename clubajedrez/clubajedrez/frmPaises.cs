﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Logicadenegocio;

namespace clubajedrez
{
    public partial class frmPaises : Form
    {
        PaisManejador _paisManejador;
        public frmPaises()
        {
            InitializeComponent();
            _paisManejador = new PaisManejador();

        }

        private void frmPaises_Load(object sender, EventArgs e)
        {
            activarBotones(false, true, false, true);
            mostrarCajas(false);
            buscar(" ");
            buscar("");
            comboPais(" ");
            comboPais("");
            comboPaisRepresenta(" ");
            comboPaisRepresenta("");

        }
        private void guardar()
        {
           
            _paisManejador.Guardar(new Pais {
                Id_Pais = Convert.ToInt32( lblID.Text),
                Nombre_de_pais=cmbPais.Text,
                Numero_de_clubs= Convert.ToInt32((txtclubs.Text)),
                Pais_que_representa=cmbPaisRepresenta.SelectedValue.ToString()
            });
        }
        private void modificar()
        {
            lblID.Text = dtgPaises.CurrentRow.Cells["Id_Pais"].Value.ToString();
            cmbPais.Text = dtgPaises.CurrentRow.Cells["Nombre_de_pais"].Value.ToString();
            txtclubs.Text = dtgPaises.CurrentRow.Cells["Numero_de_clubs"].Value.ToString();
            cmbPaisRepresenta.Text = dtgPaises.CurrentRow.Cells["Pais_que_representa"].Value.ToString();
         
        }
        private void comboPais(string filtro)
        {
            cmbPais.DataSource = _paisManejador.GetPaisR(filtro);
            cmbPais.ValueMember = "Id_Pais";
            cmbPais.DisplayMember = "Nombre_de_pais";
        }
        private void comboPaisRepresenta(string filtro)
        {
            cmbPaisRepresenta.DataSource = _paisManejador.GetPaisR(filtro);
            cmbPaisRepresenta.ValueMember = "Id_Pais";
            cmbPaisRepresenta.DisplayMember = "Nombre_de_pais";
        }
        private void eliminar()
        {
            var id = dtgPaises.CurrentRow.Cells["Id_Pais"].Value.ToString();
            _paisManejador.eliminar(Convert.ToInt32( id));
        }
        private void cmbPais_Click(object sender, EventArgs e)
        {
            comboPais(" ");
            comboPais("");
        }

        private void cmbPaisRepresenta_Click(object sender, EventArgs e)
        {
            comboPaisRepresenta(" ");
            comboPaisRepresenta("");
        }
        private void buscar(string filtro)
        {
            dtgPaises.DataSource = _paisManejador.GetPais(filtro);
        }

        private void txtBuscarpais_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscarpais.Text);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            guardar();
            buscar(" ");
            buscar("");
            limpiar();
            activarBotones(false, true, false, true);
            mostrarCajas(false);
        }

        private void dtgPaises_DoubleClick(object sender, EventArgs e)
        {
            modificar();
            mostrarCajas(true);
            activarBotones(true, false, true, false);
        }
        private void activarBotones(bool guardar, bool eliminar, bool cancelar, bool insertar)
        {
            btnGuardar.Enabled = guardar;
            btnEliminar.Enabled = eliminar;
            btnCancelar.Enabled = cancelar;
            btnInsertar.Enabled = insertar;
        }
        private void mostrarCajas(bool activar)
        {
            gpbPaises.Visible = activar;
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            mostrarCajas(true);
            activarBotones(true, false, true, false);
        }
       private void limpiar()
        {
            lblID.Text = 0.ToString();
            comboPais(" ");
            comboPais("");
            comboPaisRepresenta(" ");
            comboPaisRepresenta("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            activarBotones(false, true, false, true);
            mostrarCajas(false);
        }
    }
}
