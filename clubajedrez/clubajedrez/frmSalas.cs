﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logicadenegocio;
using Entidades;
using System.Windows.Forms;

namespace clubajedrez
{
    public partial class frmSalas : Form
    {
        private SalasManejador _salasmanejador;
        public frmSalas()
        {
            InitializeComponent();
            _salasmanejador = new SalasManejador();
        }
        private void limpiar()
        {
            lblId.Text = 0.ToString();
            txtNumero.Text = "";
            txtCapacidad.Text = "";
            txtMedios.Text = "";
        }
        private void ActivarBotones(bool insertar, bool eliminar, bool guardar)
        {
            btnInsertar.Enabled = insertar;
            btnEliminar.Enabled = eliminar;
            btnGuardar.Enabled = guardar;
        }
        private void eliminar()
        {
            var id = dtgSalas.CurrentRow.Cells["Idsala"].Value;
            _salasmanejador.Eliminar(Convert.ToInt32(id));
        }
        private void Guardar()
        {
            _salasmanejador.Guardar(new Salas
            {
                IdSala = Convert.ToInt32(lblId.Text),
                Numerosala = Convert.ToInt32(txtNumero.Text),
                Capacidad = Convert.ToInt32(txtCapacidad.Text),
                Fkhotel = cmbHotel.SelectedValue.ToString(),
                Medios = txtMedios.Text
            });

        }
        private void buscar(string filtro)
        {
            dtgSalas.DataSource = _salasmanejador.GetSalas(filtro);
        }
        private void modificar()
        {
            lblId.Text = dtgSalas.CurrentRow.Cells["Idsala"].Value.ToString();
            txtNumero.Text = dtgSalas.CurrentRow.Cells["numerosala"].Value.ToString();
            txtCapacidad.Text = dtgSalas.CurrentRow.Cells["capacidad"].Value.ToString();
            cmbHotel.Text = dtgSalas.CurrentRow.Cells["Fkhotel"].Value.ToString();
            txtMedios.Text = dtgSalas.CurrentRow.Cells["medios"].Value.ToString();
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmSalas_Load(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            gpbSalas.Visible = false;
            hotelCombo("");
            buscar("");
        }
        private void hotelCombo(string filtro)
        {
            cmbHotel.DataSource = _salasmanejador.GetHoteles(filtro);
            cmbHotel.ValueMember = "Idhotel";
            cmbHotel.DisplayMember = "nombreh";
        }

        private void cmbHotel_Click(object sender, EventArgs e)
        { 
            hotelCombo("");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true,true,false);
            Guardar();
            buscar("");
            gpbSalas.Visible = false;
            limpiar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true,true,false);
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ActivarBotones(false, false, true);
            gpbSalas.Visible = true;
            txtNumero.Enabled = true;
        }

        private void dtgSalas_DoubleClick(object sender, EventArgs e)
        {
            ActivarBotones(false,false,true);
            gpbSalas.Visible = true;
            try
            {
                modificar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true, true, false);
            limpiar();
            gpbSalas.Visible = false;
        }
    }
}
