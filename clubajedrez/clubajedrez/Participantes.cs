﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Logicadenegocio;
using System.IO;
namespace clubajedrez
{
    
    public partial class FrmParticipantes : Form
    {
        private string ruta;
        private OpenFileDialog _archivo;
        private Image Nothing = null;
        private  ParticipantesManejador _participanteManejador;
        public FrmParticipantes()
        {
            InitializeComponent();   
            this.mensaje.SetToolTip(btnImagen, "Agregar una foto");
            this.mensaje.SetToolTip(btncancelarFoto, "Eliminar foto");
            _participanteManejador = new ParticipantesManejador();
            _archivo = new OpenFileDialog();
            ruta = Application.StartupPath + "\\ PARTICIPANTES \\";
        }
        private void Participantes_Load(object sender, EventArgs e)
        {
            mostrarGrupoFoto(false);
            ActivarBotones(false, true, true, false);
            mostrarGrupoParticipantes(false);
            txtlogo.Visible = false;
            btncancelarFoto.Enabled = false;
            paisCombo(" ");
            buscar(" ");
            buscar("");  
          
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if((pblogo.Image==Nothing)&&(txtlogo.Text=="")&&(LblID.Text==0.ToString()))
            {
                MessageBox.Show("Se tiene que registrar una foto al registro");
                limpiar();
            }
            else
            {
                Guardar();
                ActivarBotones(false, true, true, false);
                mostrarGrupoParticipantes(false);
                buscar(" ");
                buscar("");
                limpiar();
            }       
        }
      
        private void Guardar()
        {       
                try
                {
                    _participanteManejador.Guardar(new Participantes
                    {
                        IdParticipante = Convert.ToInt32(LblID.Text),
                        NumeroAsociado = Convert.ToInt32(txtNumeroAsociado.Text),
                        Nombre = txtNombre.Text,
                        ApellidoP = txtApellidoPaterno.Text,
                        ApellidoM = txtApellidoMaterno.Text,
                        Direccion = txtDireccion.Text,
                        Telefono = txttelefono.Text,
                        Pais = cmbPais.SelectedValue.ToString(),
                        Foto = txtlogo.Text
                    });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n No inserto ningun dato");
                }
            guardarimagen();
            try
                {
                    if ((txtlogo.Text == "") && (gpbParticipantes.Visible == true))
                    {
                        var nombreFoto = dtgParticipantes.CurrentRow.Cells["Foto"].Value;
                        File.Delete(ruta + nombreFoto);
                    }

                }
                catch (Exception)
                {

                    MessageBox.Show("No se ha insertado una foto al participante");

                }           
        }
        private void eliminar()
        {
                var id = dtgParticipantes.CurrentRow.Cells["idparticipante"].Value;
                _participanteManejador.Eliminar(Convert.ToInt32( id));
            var nombreFoto = dtgParticipantes.CurrentRow.Cells["Foto"].Value;
            var nuevaRuta = ruta + nombreFoto;
            try
            {
                if (Directory.Exists(ruta + nombreFoto))
                {
                    File.Delete(ruta + nombreFoto);
                }
            }
            catch (Exception)
            {

                
            }         
        }
        private void MostrarFoto()
        {
            var foto = dtgParticipantes.CurrentRow.Cells["foto"].Value;
            var nombre = dtgParticipantes.CurrentRow.Cells["Nombre"].Value;
            var apellidop = dtgParticipantes.CurrentRow.Cells["ApellidoP"].Value;
            var apellidom = dtgParticipantes.CurrentRow.Cells["ApellidoM"].Value;          
            LbNombrel.Text = nombre.ToString() + " " +  apellidop.ToString() +" "+ apellidom.ToString();
            var variable = ruta + foto;       
            _archivo.FileName = variable;
            string direccion = _archivo.FileName;
            PBFotoM.ImageLocation = direccion;
            PBFotoM.SizeMode = PictureBoxSizeMode.StretchImage;        
          
        }     
        private void modificar()
        {
            LblID.Text = dtgParticipantes.CurrentRow.Cells["IdParticipante"].Value.ToString();
            txtNumeroAsociado.Text=dtgParticipantes.CurrentRow.Cells["NumeroAsociado"].Value.ToString();
            txtNombre.Text= dtgParticipantes.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidoPaterno.Text= dtgParticipantes.CurrentRow.Cells["ApellidoP"].Value.ToString();
            txtApellidoMaterno.Text= dtgParticipantes.CurrentRow.Cells["ApellidoM"].Value.ToString();
            txtDireccion.Text= dtgParticipantes.CurrentRow.Cells["Direccion"].Value.ToString();
            txttelefono.Text= dtgParticipantes.CurrentRow.Cells["Telefono"].Value.ToString();
            cmbPais.Text= dtgParticipantes.CurrentRow.Cells["Pais"].Value.ToString();
            txtlogo.Text = dtgParticipantes.CurrentRow.Cells["foto"].Value.ToString();
            string direccion = ruta + txtlogo.Text;
            pblogo.ImageLocation = direccion;
            pblogo.SizeMode = PictureBoxSizeMode.StretchImage;          
        }
        private void validarFoto()
        {         
            if (txtlogo.Text !="")
            {
                btncancelarFoto.Enabled = true;
            }
            else
            {
                btncancelarFoto.Enabled = false;
            }
        }
        private void paisCombo(string filtro)
        {
            cmbPais.DataSource = _participanteManejador.GetPaies(filtro) ;
            cmbPais.ValueMember = "Id_Pais";
            cmbPais.DisplayMember = "Nombre_de_pais";
        
        }
        private void buscar(string filtro)
        {
            dtgParticipantes.DataSource = _participanteManejador.GetParticipantes(filtro);
        }  
        private void cmbPais_Click(object sender, EventArgs e)
        {
            paisCombo(" ");
            paisCombo("");
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }
        private void dtgParticipantes_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                mostrarGrupoParticipantes(true);
                ActivarBotones(true, false, false, true);
                modificar();     
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }
        private void limpiar()
        {
            LblID.Text = 0.ToString();
            txtNumeroAsociado.Text = "";
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtDireccion.Text = "";
            txttelefono.Text = "";
            cmbPais.Text = "";
            paisCombo(" ");
            paisCombo("");
            pblogo.Image = Nothing;
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            ActivarBotones(false, true, true, false);
            mostrarGrupoParticipantes(false);       
        }   
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminar();
                    buscar("");  
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }
        private void btnImagen_Click(object sender, EventArgs e)
        {
            _archivo.Filter =" Imagen tipo(*.jpg) | *.jpg | (*.png) | *.png";
            _archivo.Title = "cargar imagen";
            _archivo.ShowDialog();
            string direccion = _archivo.FileName;
            pblogo.ImageLocation = direccion;
            pblogo.SizeMode = PictureBoxSizeMode.StretchImage;
         
            if (_archivo.FileName != "")
            {
                var archivo = new FileInfo(_archivo.FileName);
                txtlogo.Text = archivo.Name;
            }

        }
        private void txtlogo_TextChanged(object sender, EventArgs e)
        {
            validarFoto();
           
        }
        private void guardarimagen()
        {                     
            try
            {
                if (_archivo.FileName != null)
                {
                    if (_archivo.FileName != "")
                    {
                        var document = new FileInfo(_archivo.FileName);
                        if (Directory.Exists(ruta))
                        {
                            //codigo para aggregar el archivo
                            if (Directory.Exists(ruta) == false)
                            {
                                var obtenerArchivps = Directory.GetFiles(ruta, _archivo.Filter);
                        
                                FileInfo archivoanterior;
                                if (obtenerArchivps.Length != 0)
                                {
                                    //codigo para remplazar imagen
                                    archivoanterior = new FileInfo(obtenerArchivps[0]);
                                    if (archivoanterior.Exists)
                                    {
                                        archivoanterior.Delete();
                                        document.CopyTo(ruta + document.Name);                                       
                                    }
                                }
                            }
                            else
                            {
                                document.CopyTo(ruta + document.Name);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(ruta);
                            document.CopyTo(ruta + document.Name);
                        }
                    }
                }
            }
            catch (Exception )
            {
            }
        }
        private void btncancelarFoto_Click(object sender, EventArgs e)
        {
        
            if (MessageBox.Show("¿Estas seguro que deceas eliminar la imagen?, despues tendras que insertar una foto. Los cambios se haran al guardar el registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    txtlogo.Text = "";
                    pblogo.Image = Nothing;         
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ActivarBotones(true, false, false, true);
            mostrarGrupoParticipantes(true);
        }
        private void mostrarGrupoParticipantes(bool grupoParticipantes)
        {
            gpbParticipantes.Visible = grupoParticipantes;
        }
        private void mostrarGrupoFoto(bool grupoFoto)
        {
            GpoFoto.Visible = grupoFoto;
        }
        private void ActivarBotones(bool guardar,bool eliminar,bool insertar,bool cancelar)
        {
            btnInsertar.Enabled = insertar;
            btnEliminar.Enabled = eliminar;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
        }
        private void activardtgParticipantes(bool participantes)
        {
            dtgParticipantes.Enabled = participantes;
        }
        private void ActivarGrupos(bool gParticipantes,bool gFoto)
        {
            gpbParticipantes.Enabled = gParticipantes;
            GpoFoto.Enabled = gFoto;
        }
        private void cmbPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Ignorar pero no borrar 
        }
        private void dtgParticipantes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Ignorar pero no borrar 
        }
        private void dtgParticipantes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Ignorar pero no borrar 
        }
        private void dtgParticipantes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {          
            MostrarFoto();
            mostrarGrupoFoto(true);
            if ((GpoFoto.Visible == false)&&(dtgParticipantes.Visible==true))
            {
                ActivarBotones(true, false, false, true);
                mostrarGrupoFoto(false);
            }
            else
            {
                ActivarBotones(false, false, false, false);
            }
             if((GpoFoto.Visible == true)&&(gpbParticipantes.Visible==false))
            {
                ActivarBotones(false, false, false, false);
            }

              if(gpbParticipantes.Visible==true)
            {           
                ActivarBotones(true, false, false, true);
                activardtgParticipantes(true);
                mostrarGrupoFoto(false);
            }
        }
        private void FrmParticipantes_Click(object sender, EventArgs e)
        {
            mostrarGrupoFoto(false);
           if(GpoFoto.Visible==true)
            {
                activardtgParticipantes(false);
                ActivarBotones(false, false, false, false);

            }
           else
            {
                activardtgParticipantes(true);
                ActivarBotones(false, true, true, false);
            }
            if (gpbParticipantes.Visible == true)
            {
                ActivarBotones(true, false, false, true);                      
            }                         
        }
        private void FrmParticipantes_MouseClick(object sender, MouseEventArgs e)
        {
            //Ignorar pero no borrar 
        }
        private void dtgParticipantes_MouseHover(object sender, EventArgs e)
        {
            //Ignorar pero no borrar 
        }
        private void dtgParticipantes_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            string newLine = Environment.NewLine;
            if (e.RowIndex > -1)
            {

                DataGridViewRow dataGridViewRow1 = dtgParticipantes.Rows[e.RowIndex];             
                if((GpoFoto.Visible==false)&&(gpbParticipantes.Visible==false))
                { 
                    e.ToolTipText = String.Format("Click sobre texto para mostrar foto del participante {0}{1} Doble click para editar datos del participante {0}{1}",
                   dataGridViewRow1.Cells["idparticipante"].Value,newLine,ToolTipIcon.Info);
                }
            }
        }
        private void gpbParticipantes_Enter(object sender, EventArgs e)
        {
        }
        private void btncerrarImagen_Click(object sender, EventArgs e)
        {
            GpoFoto.Visible = false;
            ActivarBotones(false, true, true, false);
            activardtgParticipantes(true);
        }
        private void dtgParticipantes_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            //Ignorar pero no borrar 
        }
        private void dtgParticipantes_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            //Ignorar pero no borrar 
        }

        private void dtgParticipantes_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                mostrarGrupoParticipantes(true);
                ActivarBotones(true, false, false, true);
                modificar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
