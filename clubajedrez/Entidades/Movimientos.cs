﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Movimientos
    {
        private int _idMovimiento;
        private int _numeroMovimiento;
        private string _posiciones;
        private string _comentario;
        private string _partida;

        public int IdMovimiento { get => _idMovimiento; set => _idMovimiento = value; }
        public int NumeroMovimiento { get => _numeroMovimiento; set => _numeroMovimiento = value; }
        public string Posiciones { get => _posiciones; set => _posiciones = value; }
        public string Comentario { get => _comentario; set => _comentario = value; }
        public string Partida { get => _partida; set => _partida = value; }
    }
}
