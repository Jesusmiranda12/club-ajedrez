﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Arbitros
    {
        private int _idArbitro;
        private string numero_asociado;
        private string nombreArbitro;
        private string apelldoP;
        private string apellidoM;
        private string direccion;
        private string telefono;
        private string foto;

        public int IdArbitro { get => _idArbitro; set => _idArbitro = value; }
        public string Numero_asociado { get => numero_asociado; set => numero_asociado = value; }
        public string NombreArbitro { get => nombreArbitro; set => nombreArbitro = value; }
        public string ApelldoP { get => apelldoP; set => apelldoP = value; }
        public string ApellidoM { get => apellidoM; set => apellidoM = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Foto { get => foto; set => foto = value; }
    }
}
