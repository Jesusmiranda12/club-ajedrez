﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Hoteles
    {
        private int _idHotel;
        private string _nombreh;
        private string _direccion;
        private string _telefono;

        public int IdHotel { get => _idHotel; set => _idHotel = value; }
        public string Nombreh { get => _nombreh; set => _nombreh = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
    }
}
