﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Salas
    {
        private int _idSala;
        private int _numerosala;
        private int _capacidad;
        private string _fkhotel;
        private string _medios;

        public int IdSala { get => _idSala; set => _idSala = value; }
        public int Numerosala { get => _numerosala; set => _numerosala = value; }
        public int Capacidad { get => _capacidad; set => _capacidad = value; }
        public string Fkhotel { get => _fkhotel; set => _fkhotel = value; }
        public string Medios { get => _medios; set => _medios = value; } 
    }
}
