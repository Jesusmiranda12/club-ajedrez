﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
   public class Hospedajes
    {
        private int _idHospedaje;
        private string _numeroAsociado;
        private string _nombrehotel;
        private string _fechaEntrada;
        private string _fechaSalida;   
        private string _nombreParticipante;
        private string _apellidoP;
        private string _apellidoM;
        private string _direccionparticipante;
        private string _telefonoparticipante;     
        private string _direccionhotel;
        private string _telefonohotel;

        public int IdHospedaje { get => _idHospedaje; set => _idHospedaje = value; }
        public string NumeroAsociado { get => _numeroAsociado; set => _numeroAsociado = value; }
        public string Nombrehotel { get => _nombrehotel; set => _nombrehotel = value; }
        public string FechaEntrada { get => _fechaEntrada; set => _fechaEntrada = value; }
        public string FechaSalida { get => _fechaSalida; set => _fechaSalida = value; }
        public string NombreParticipante { get => _nombreParticipante; set => _nombreParticipante = value; }
        public string ApellidoP { get => _apellidoP; set => _apellidoP = value; }
        public string ApellidoM { get => _apellidoM; set => _apellidoM = value; }
        public string Direccionparticipante { get => _direccionparticipante; set => _direccionparticipante = value; }
        public string Telefonoparticipante { get => _telefonoparticipante; set => _telefonoparticipante = value; }
        public string Direccionhotel { get => _direccionhotel; set => _direccionhotel = value; }
        public string Telefonohotel { get => _telefonohotel; set => _telefonohotel = value; }
    }
}
