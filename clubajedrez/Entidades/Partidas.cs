﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
   public class Partidas
    {
        private int _idpartida;
        private string _jugadorBlancas;
        private string _jugadorNegras;
        private string _arbitro;
        private int _sala;
        private string _fecha;

        public int Idpartida { get => _idpartida; set => _idpartida = value; }
        public string JugadorBlancas { get => _jugadorBlancas; set => _jugadorBlancas = value; }
        public string JugadorNegras { get => _jugadorNegras; set => _jugadorNegras = value; }
        public string Arbitro { get => _arbitro; set => _arbitro = value; }
        public int Sala { get => _sala; set => _sala = value; }
        public string Fecha { get => _fecha; set => _fecha = value; }      
    }
}
