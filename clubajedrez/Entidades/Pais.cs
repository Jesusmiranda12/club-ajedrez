﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Pais
    {
        private int _id_Pais;
        private string _nombre_de_pais;
        private int _numero_de_clubs;
        private string _pais_que_representa;

        public int Id_Pais { get => _id_Pais; set => _id_Pais = value; }
        public string Nombre_de_pais { get => _nombre_de_pais; set => _nombre_de_pais = value; }
        public int Numero_de_clubs { get => _numero_de_clubs; set => _numero_de_clubs = value; }
        public string Pais_que_representa { get => _pais_que_representa; set => _pais_que_representa = value; }
    }
}
