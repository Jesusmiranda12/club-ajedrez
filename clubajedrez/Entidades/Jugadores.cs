﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Jugadores
    {
        private int _idjugador;
        private string _numeroasociado;
        private string _nombre;
        private string _apellidop;
        private string _apellidom;
        private string _direccion;
        private string _pais;
        private int _nivel;
        private string _foto;

        public int Idjugador { get => _idjugador; set => _idjugador = value; }
        public string Numeroasociado { get => _numeroasociado; set => _numeroasociado = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellidop { get => _apellidop; set => _apellidop = value; }
        public string Apellidom { get => _apellidom; set => _apellidom = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Pais { get => _pais; set => _pais = value; }
        public int Nivel { get => _nivel; set => _nivel = value; }
        public string Foto { get => _foto; set => _foto = value; }
    }
}
