create database clubajedrez;
use clubajedrez;
#region crer tablas
/*PAISES*/
create table paises(idpais int AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(50), numeroclubs int, fkpaisrepresentante int,
FOREIGN KEY (fkpaisrepresentante) REFERENCES paises(idpais)on delete cascade on update cascade);
/*PARTICIPANTES*/
create table participantes(idparticipante int auto_increment PRIMARY KEY, numero_asociado varchar(10), nombre varchar(100), 
apellidop varchar(100), apellidom varchar(100), direccion varchar(150), telefono varchar(10), paisquerepresenta int, foto varchar(70),
foreign key(paisquerepresenta) references paises(idpais) on delete cascade on update cascade);
/*JUGADORES*/
create table jugadores(idjugador int primary key, nivel int,
foreign key (idjugador) references participantes(idparticipante)on delete cascade on update cascade);
/*ARBITROS*/
create table arbitros(idarbitro int primary key, foreign key(idarbitro) references participantes(idparticipante)on delete cascade on update cascade);
/*HOTELES*/
create table hoteles(Idhotel int primary key auto_increment, nombreh varchar(100), direccion varchar(100), 
telefono varchar(100));
/*SALAS*/
create table salas(Idsala int primary key auto_increment, numerosala int, capacidad int, fkhotel int, medios varchar(200),
foreign key(fkhotel) references hoteles(Idhotel)on delete cascade on update cascade);
/*HOSPEDAJES*/
create table hospedajes( id int auto_increment primary key, fkparticipante int, fkhotel int, fechaentrada varchar(50),
fechasalida varchar(50), foreign key (fkparticipante) references participantes(idparticipante)on update cascade on delete cascade,
foreign key (fkhotel)references hoteles(idhotel)on delete cascade on update cascade);
/*PARTIDAS*/
create table partidas( idpartida int auto_increment primary key, jugadorblancas varchar(10), jugadornegras varchar(10), arbitro varchar(10), sala int,
fecha varchar(50), FOREIGN KEY (sala) REFERENCES salas(idsala)on delete cascade on update cascade);
/*MOVIMIENTOS*/
create table movimientos( idmovimiento int auto_increment primary key, numerodemovimiento int, posiciones varchar(20),
comentario varchar(50), fkpartida int, FOREIGN KEY (fkpartida) REFERENCES partidas(idpartida)on delete cascade on update cascade);
#end region
#region procedures
#region Paises
create procedure p_insertarPaises(in _id int, in _nombre varchar(50), in _numeroclubs int, in _fkpaisrepresentante int)
begin 
insert into paises values(null, _nombre , _numeroclubs, _fkpaisrepresentante);
end;
create procedure p_eliminarPaises(in _id int)
begin
delete from paises where idpais = _id;
end;
create procedure p_actualizarPaises(in _nombre varchar(50), in _numeroclubs int, in _fkpaisrepresentante int, in _id int)
begin
update paises set nombre = _nombre, numeroclubs = _numeroclubs, fkpaisrepresentante = _fkpaisrepresentante where idpais = _id;
end;
create procedure p_verNombrePais(in _filtro varchar(50))
begin
select * from paises where nombre like concat('%',_filtro,'%');
end;
create procedure p_verPaises(in _filtro varchar(50))
begin
select p.idpais,p.nombre,p.numeroclubs,paises.nombre as 'PaisRepresenta' from paises p inner join paises ON paises.idpais = p.fkpaisrepresentante   where  p.nombre like concat('%',_filtro,'%');
end;
#end region 
#region participantes
create procedure p_insertarParticipantes(in _id int,in _numero_asociado varchar(10), in _nombre varchar(100), _apellidop varchar(100),
_apellidom varchar(100), _direccion varchar(150), _telefono varchar(10), in _pais int,in _foto varchar(70))
begin
insert into participantes values(null,_numero_asociado,_nombre,_apellidop,_apellidom,_direccion,_telefono,_pais,_foto);
end;
create procedure p_eliminarParticipante(in _id int)
begin
delete from participantes where idparticipante = _id;
end;
create PROCEDURE p_actualizarParticipante(in _numero_asociado varchar(10), in _nombre varchar(100), _apellidop varchar(100),
_apellidom varchar(100), _direccion varchar(150), _telefono varchar(10), in _pais int,in _foto varchar(70),in id int)
begin
update participantes set numero_asociado = _numero_asociado,nombre = _nombre,apellidop=_apellidop,apellidom=_apellidom,
direccion = _direccion,telefono = _telefono, paisquerepresenta=_pais,foto=_foto where idparticipante = id;
end;
create procedure p_verParticipantes(in _idparticipante int, in _numeroAsociado varchar(10))
begin
select * from participantes where (
case
when _idparticipante >0 then
idparticipante = _idparticipante
else 1=1
end) and
(case
when _numeroAsociado <> '' then
numero_asociado like concat('%',_numeroAsociado,'%')
else 1=1
end);
end;

#end region 
#region jugadores
create procedure p_insertarJugadores(in _idJugador int,in _nivel int)
begin
declare x int;
select count(idarbitro) from arbitros where idarbitro =_idJugador into x;
if x=0 then
insert into jugadores values(_idJugador,_nivel);
else
select 'Este participante ya ha sido registrado como arbitro' mensaje;
end if;
end;
create procedure p_eliminarJugador(in _idjugador int)
begin
delete from jugadores where idjugador=_idjugador;
end;
create PROCEDURE p_actualizarJugadores(in _nivel int,in _idjugador int)
begin
update jugadores set nivel = _nivel where idjugador = _idjugador;
end;
#end region 
#region arbitros 
create procedure p_insertarArbitros(in _idarbitro int)
begin
declare x int;
select count(idjugador) from jugadores where idjugador =_idarbitro into x;
if x=0 then
insert into arbitros values(_idarbitro);
else
select 'Este participante ya ha sido registrado como jugador' mensaje;
end if;
end;
create procedure p_eliminarArbitro(in _idarbitro int)
begin
delete from arbitros where idarbitro=_idarbitro;
end;
create procedure p_actualizarArbitro(in _idarbitro int)
begin
update arbitros set idarbitro = _idarbitro where idarbitro;
end;
#end region 
#region Hoteles 
create procedure p_insertarHoteles(in _id int, in _nombreh varchar(101), in _direccion varchar(100), in _telefono varchar(10))
begin 
insert into hoteles values(null, _nombreh , _direccion, _telefono);
end;
create procedure p_eliminarHoteles(in _id int)
begin
delete from Hoteles where Idhotel = _id;
end;
create procedure p_actualizarHoteles(in _nombreh varchar(100), in _direccion varchar(100), in _telefono varchar(10), in _id int)
begin
update hoteles set nombreh = _nombreh, direccion = _direccion, telefono = _telefono where Idhotel = _id;
end;
create procedure p_verHoteles(in _filtro varchar(50))
begin
select * from hoteles where nombreh like concat('%',_filtro,'%');
end;
#end region 
#region Salas
create procedure p_insertarSalas(in _id int,in _numerosala int, in _capacidad int, in _fkhotel int, in _medios varchar(200))
begin 
insert into salas values(null, _numerosala , _capacidad, _fkhotel, _medios);
end;
create procedure p_eliminarSalas(in _id int)
begin
delete from salas where Idsala = _id;
end;
create procedure p_actualizarSalas(in _numerosala varchar(100), in _capacidad varchar(100),in _fkhotel int,
in _medios varchar(200),in _id int)
begin
update salas set numerosala = _numerosala, capacidad = _capacidad, fkhotel = _fkhotel, medios = _medios
where Idsala = _id;
end;
#end region 
#region hospedajes
create procedure p_insertarHospedajes(in _id int, in _participante int, in _hotel int, in _fechaentrada varchar(50),
in _fechasalida varchar(50))
begin
insert into hospedajes values(null,_participante,_hotel, _fechaentrada,_fechasalida);
end;
create procedure p_eliminarHospedaje(in _id int)
begin
delete from hospedajes where id = _id;
end;
create procedure p_actualizarHospedajes(in _participante int, in _hotel int, in _fechaentrada varchar(50),
in _fechasalida varchar(50),in _id int)
begin
update hospedajes set fkparticipante= _participante,fkhotel=_hotel,fechaentrada =_fechaentrada,fechasalida = _fechasalida
where id=_id;
end;
#end region 
#region Partidas
create procedure p_insertarPartidas(in _idpartida int,in _jugadorblancas varchar(10),in _jugadornegras varchar(10), in _arbitro varchar(10),
in _sala int, in _fecha varchar(50))
begin
insert into partidas values(null,_jugadorblancas,_jugadornegras,_arbitro,_sala,_fecha);
end;
create procedure p_eliminarPartidas(in _id int)
begin
delete from partidas where idpartida = _id;
end;
create procedure p_actualizarPartidas(in _jugadorblancas varchar(10),in _jugadornegras varchar(10), in _arbitro varchar(10),
in _sala int, in _fecha varchar(50),in _id int)
begin
update partidas set jugadorblancas=_jugadorblancas,jugadornegras=_jugadornegras,arbitro=_arbitro,sala=_sala,fecha=_fecha
where idpartida=_id ;
end;
create procedure p_verIdPartida()
begin
select idpartida from partidas;
end;
#end region 
#region movimientos
create procedure p_insertarMovimientos(in _id int, in _numerodemovimiento int, in _posiciones varchar(20), in _comentario varchar(50),
in _fkpartida int)
begin 
insert into movimientos values(null, _numerodemovimiento, _posiciones, _comentario, _fkpartida);
end;
create procedure p_eliminarMovimientos(in _id int)
begin
delete from movimientos where idmovimiento = _id;
end;
create procedure p_actualizarMovimientos(in _numerodemovimiento int, in _posiciones varchar(20), in _comentario varchar(50),
in _fkpartida int, in _id int)
begin
update movimientos set numerodemovimiento = _numerodemovimiento, posiciones = _posiciones, comentario = _comentario, 
fkpartida = _fkpartida where idmovimiento = _id;
end;
#end region
#end region
#region JOIN
/*PARTICIPANTESCONPAIS*/
create procedure p_verParticipantesconpais(in _filtro varchar(10))
begin
select p.idparticipante, p.numero_asociado, p.nombre, p.apellidop, p.apellidom, p.direccion, p.telefono, pa.nombre as 'pais_que_representa',p.foto from participantes p inner join paises pa on p.paisquerepresenta=pa.idpais  where p.numero_asociado like concat('%',_filtro,'%');/*INNER JOIN*/
end;
/*HOSPEDAJES*/
create procedure p_verHospedajes(in _filtro varchar(10))
begin
select h.id, p.numero_asociado, p.nombre as 'NombreParticipante', p.apellidop, p.apellidom, p.direccion, p.telefono, ho.nombreh, ho.direccion as 'Direccion de hotel', ho.telefono as 'Telefono del hotel', h.fechaentrada, h.fechasalida from hospedajes h inner join participantes p inner join hoteles ho on h.fkhotel = ho.Idhotel and h.fkparticipante = p.idparticipante where p.numero_asociado like concat('%',_filtro,'%');/*INNER JOIN*/
end;
/*JUGADORES*/
create procedure p_verJugadores(in _filtro varchar(10))
begin
select p.idparticipante, p.numero_asociado, p.nombre, p.apellidop, p.apellidom, p.direccion, pa.nombre as 'Pais que representa', j.nivel, p.foto from participantes p inner join paises pa inner join jugadores j on p.idparticipante = pa.idpais and j.idjugador = p.idparticipante where p.numero_asociado like concat('%',_filtro,'%');
end;
/*SALAS*/
create procedure p_verSalas(in _filtro int)
begin 
select s.Idsala,s.numerosala,s.capacidad,h.nombreh,s.medios from salas s inner join hoteles h on s.fkhotel = h.Idhotel where h.nombreh = _filtro;
end;
/*MOVIMIENTOS*/
create procedure p_verMovimientos(in _filtro varchar(10))
begin 
select m.idmovimiento,m.numerodemovimiento,m.posiciones,m.comentario, p.idpartida as 'Partida' from movimientos m inner join partidas p on m.fkpartida = p.idpartida where m.numerodemovimiento like concat('%',_filtro,'%');
end;
/*ARBITROS*/
create procedure p_verArbitros(in _filtro varchar(50))
begin 
select a.idarbitro, p.numero_asociado, p.nombre, p.apellidop, p.apellidom, p.direccion, p.telefono, p.foto from participantes p inner join arbitros a on a.idarbitro = p.idparticipante where p.nombre like concat('%',_filtro,'%');
end;
/*PARTIDAS*/
create procedure p_verPartidas(in _filtro varchar(50))
begin
select p.idpartida, p.jugadorblancas , p.jugadornegras , p.arbitro, s.numerosala, p.fecha from partidas p inner join salas s
on p.sala = s.Idsala where p.fecha like concat('%',_filtro,'%');
end;
#region insertacion de paises
call p_insertarPaises(null,' Afganistán', null,null); 
call p_insertarPaises(null,' Albania', null,null);
call p_insertarPaises(null,' Alemania', null,null);
call p_insertarPaises(null,' Andorra', null,null);
call p_insertarPaises(null,'Angola', null,null);
call p_insertarPaises(null,' Antigua y Barbuda', null,null);
call p_insertarPaises(null,' Arabia Saudita', null,null);
call p_insertarPaises(null,' Argelia', null,null);
call p_insertarPaises(null,'Argentina', null,null);
call p_insertarPaises(null,'Armenia', null,null);
call p_insertarPaises(null,'Australia', null,null);
call p_insertarPaises(null,'Austria', null,null);
call p_insertarPaises(null,' Azerbaiyán', null,null);
call p_insertarPaises(null,' Bahamas', null,null);
call p_insertarPaises(null,' Bangladés', null,null);
call p_insertarPaises(null,' Barbados', null,null);
call p_insertarPaises(null,' Baréin', null,null);
call p_insertarPaises(null,' Bélgica', null,null);
call p_insertarPaises(null,' Belice', null,null);
call p_insertarPaises(null,' Benín', null,null);
call p_insertarPaises(null,' Bielorrusia', null,null);
call p_insertarPaises(null,' Birmania Myanmar', null,null);
call p_insertarPaises(null,' Bolivia', null,null);
call p_insertarPaises(null,' Bosnia y Herzegovina', null,null);
call p_insertarPaises(null,' Botsuana', null,null);
call p_insertarPaises(null,' Brasil', null,null);
call p_insertarPaises(null,' Brunéi', null,null);
call p_insertarPaises(null,' Bulgaria', null,null);
call p_insertarPaises(null,' Burkina Faso', null,null);
call p_insertarPaises(null,' Burundi', null,null);
call p_insertarPaises(null,' Bután', null,null);
call p_insertarPaises(null,' Cabo Verde', null,null);
call p_insertarPaises(null,' Camboya', null,null);
call p_insertarPaises(null,' Camerún', null,null);
call p_insertarPaises(null,' Canadá', null,null);
call p_insertarPaises(null,' Catar', null,null);
call p_insertarPaises(null,' Chad', null,null);
call p_insertarPaises(null,' Chile', null,null);
call p_insertarPaises(null,' China', null,null);
call p_insertarPaises(null,' Chipre', null,null);
call p_insertarPaises(null,' Ciudad del Vaticano', null,null);
call p_insertarPaises(null,' Colombia', null,null);
call p_insertarPaises(null,' Comoras', null,null);
call p_insertarPaises(null,' Corea del Norte', null,null);
call p_insertarPaises(null,' Corea del Sur', null,null);
call p_insertarPaises(null,' Costa de Marfil', null,null);
call p_insertarPaises(null,' Costa Rica', null,null);
call p_insertarPaises(null,' Croacia', null,null);
call p_insertarPaises(null,' Cuba', null,null);
call p_insertarPaises(null,' Dinamarca', null,null);
call p_insertarPaises(null,' Dominica', null,null);
call p_insertarPaises(null,' Ecuador', null,null);
call p_insertarPaises(null,' Egipto', null,null);
call p_insertarPaises(null,' El Salvador', null,null);
call p_insertarPaises(null,' Emiratos Árabes Unidos', null,null);
call p_insertarPaises(null,' Eritrea', null,null);
call p_insertarPaises(null,' Eslovaquia', null,null); 
call p_insertarPaises(null,' Eslovenia', null,null);
call p_insertarPaises(null,' España', null,null);
call p_insertarPaises(null,' Estados Unidos', null,null);
call p_insertarPaises(null,' Estonia', null,null);
call p_insertarPaises(null,' Etiopía ', null,null);
call p_insertarPaises(null,' Filipinas ', null,null);
call p_insertarPaises(null,' Finlandia ', null,null);
call p_insertarPaises(null,' Fiyi ', null ,null);
call p_insertarPaises(null,' Francia ', null,null);
call p_insertarPaises(null,' Gabón ', null,null);
call p_insertarPaises(null,' Gambia ', null,null);
call p_insertarPaises(null,' Georgia ', null,null);
call p_insertarPaises(null,' Ghana ', null,null);
call p_insertarPaises(null,' Granada', null,null);
call p_insertarPaises(null,' Grecia ', null,null);
call p_insertarPaises(null,' Guatemala ', null,null);
call p_insertarPaises(null,' Guyana ', null,null);
call p_insertarPaises(null,' Guinea ', null,null);
call p_insertarPaises(null,' Guinea ecuatorial ', null,null);
call p_insertarPaises(null,' Guinea-Bisáu', null,null);
call p_insertarPaises(null,' Haití ', null,null);
call p_insertarPaises(null,' Honduras ', null,null);
call p_insertarPaises(null,' Hungría ', null,null);
call p_insertarPaises(null,' India ', null,null);
call p_insertarPaises(null,' Indonesia ', null,null);
call p_insertarPaises(null,' Irak', null,null);
call p_insertarPaises(null,' Irán ', null,null);
call p_insertarPaises(null,' Irlanda ', null,null);
call p_insertarPaises(null,' Islandia ', null,null);
call p_insertarPaises(null,' Islas Marshall', null,null);
call p_insertarPaises(null,' Islas Salomón',null,null);
call p_insertarPaises(null,' Israel ',null,null);
call p_insertarPaises(null,' Italia',null,null);
call p_insertarPaises(null,' Jamaica',null,null);
call p_insertarPaises(null,' Japón',null,null);
call p_insertarPaises(null,' Jordania',null,null);
call p_insertarPaises(null,' Kazajistán',null,null);
call p_insertarPaises(null,' Kenia',null,null);
call p_insertarPaises(null,' Kirguistán',null,null);
call p_insertarPaises(null,' Kiribati',null,null);
call p_insertarPaises(null,' Kuwait',null,null);
call p_insertarPaises(null,' Laos',null,null);
call p_insertarPaises(null,' Lesoto',null,null);
call p_insertarPaises(null,' Letonia',null,null);
call p_insertarPaises(null,' Líbano',null,null);
call p_insertarPaises(null,' Liberia',null,null);
call p_insertarPaises(null,' Libia',null,null);
call p_insertarPaises(null,' Liechtenstein',null,null);
call p_insertarPaises(null,' Lituania',null,null);
call p_insertarPaises(null,' Luxemburgo',null,null);
call p_insertarPaises(null,' Macedonia del Norte',null,null);
call p_insertarPaises(null,' Madagascar',null,null);
call p_insertarPaises(null,' Malasia',null,null);
call p_insertarPaises(null,' Malaui',null,null);
call p_insertarPaises(null,' Maldivas',null,null);
call p_insertarPaises(null,' Malí',null,null);
call p_insertarPaises(null,' Malta',null,null);
call p_insertarPaises(null,' Marruecos',null,null);
call p_insertarPaises(null,' Mauricio',null,null);
call p_insertarPaises(null,' Mauritania',null,null);
call p_insertarPaises(null,' México',null,null);
call p_insertarPaises(null,' Micronesia',null,null);
call p_insertarPaises(null,' Moldavia',null,null);
call p_insertarPaises(null,' Mónaco',null,null);
call p_insertarPaises(null,' Mongolia',null,null);
call p_insertarPaises(null,' Montenegro',null,null);
call p_insertarPaises(null,' Mozambique',null,null);
call p_insertarPaises(null,' Namibia',null,null);
call p_insertarPaises(null,' Nauru',null,null);
call p_insertarPaises(null,' Nepal',null,null);
call p_insertarPaises(null,' Nicaragua',null,null);
call p_insertarPaises(null,' Níger',null,null);
call p_insertarPaises(null,' Nigeria',null,null);
call p_insertarPaises(null,' Noruega',null,null);
call p_insertarPaises(null,' Nueva Zelanda',null,null);
call p_insertarPaises(null,' Omán',null,null);
call p_insertarPaises(null,' Países Bajos', null,null);
call p_insertarPaises(null,' Pakistán', null,null);
call p_insertarPaises(null,' Palaos ', null,null);
call p_insertarPaises(null,' Panamá', null,null);
call p_insertarPaises(null,' Papúa Nueva Guinea', null,null);
call p_insertarPaises(null,' Paraguay ', null,null);
call p_insertarPaises(null,' Perú ', null,null);
call p_insertarPaises(null,' Polonia', null,null);
call p_insertarPaises(null,' Portugal', null,null);
call p_insertarPaises(null,' Reino Unido', null,null);
call p_insertarPaises(null,' República Centroafricana ', null,null);
call p_insertarPaises(null,' República Checa ', null,null);
call p_insertarPaises(null,' República del Congo ', null,null);
call p_insertarPaises(null,' República Democrática del Congo', null,null);
call p_insertarPaises(null,' República Dominicana', null,null);
call p_insertarPaises(null,' República Sudafricana ', null,null);
call p_insertarPaises(null,' Ruanda ', null,null);
call p_insertarPaises(null,' Rumanía ', null,null);
call p_insertarPaises(null,' Rusia ', null,null);
call p_insertarPaises(null,' Samoa ', null,null);
call p_insertarPaises(null,' Santa Lucía ', null,null);
call p_insertarPaises(null,' Santo Tomé y Príncipe ', null,null);
call p_insertarPaises(null,' San Vicente y las Granadinas ', null,null);
call p_insertarPaises(null,' San Marino ', null,null);
call p_insertarPaises(null,' San Cristóbal y Nieves ', null,null);
call p_insertarPaises(null,' Senegal ', null,null);
call p_insertarPaises(null,' Serbia ', null,null);
call p_insertarPaises(null,' Seychelles', null,null);
call p_insertarPaises(null,' Sierra Leona ', null,null);
call p_insertarPaises(null,' Singapur ', null,null);
call p_insertarPaises(null,' Siria', null,null);
call p_insertarPaises(null,' Somalia', null,null);
call p_insertarPaises(null,' Sri Lanka ', null,null);
call p_insertarPaises(null,' Suazilandia ', null,null);
call p_insertarPaises(null,' Sudán', null,null);
call p_insertarPaises(null,' Sudán del Sur ', null,null);
call p_insertarPaises(null,' Suecia ', null,null);
call p_insertarPaises(null,' Suiza ', null,null);
call p_insertarPaises(null,' Surinam ', null,null);
call p_insertarPaises(null,' Tanzania ', null,null);
call p_insertarPaises(null,' Tailandia ', null,null);
call p_insertarPaises(null,' Tanzania ', null,null);
call p_insertarPaises(null,' Tayikistán ', null,null);
call p_insertarPaises(null,' Timor Oriental ', null,null);
call p_insertarPaises(null,' Togo ', null,null);
call p_insertarPaises(null,' Tonga ', null,null);
call p_insertarPaises(null,' Trinidad y Tobago ', null,null);
call p_insertarPaises(null,' Túnez ', null,null);
call p_insertarPaises(null,' Turkmenistán ', null,null);
call p_insertarPaises(null,' Turquía ', null,null);
call p_insertarPaises(null,' Tuvalu', null,null);
call p_insertarPaises(null,' Ucrania', null,null);
call p_insertarPaises(null,' Uganda ', null,null);
call p_insertarPaises(null,' Uruguay ', null,null);
call p_insertarPaises(null,' Uzbekistán ', null,null);
call p_insertarPaises(null,' Vanuatu ', null,null);
call p_insertarPaises(null,' Venezuela ', null,null);
call p_insertarPaises(null,' Vietnam ', null,null);
call p_insertarPaises(null,' Yemen ', null,null);
call p_insertarPaises(null,' Yibuti ', null,null);
call p_insertarPaises(null,' Zambia ', null,null);
call p_insertarPaises(null,' Zimbabue', null,null);
#end region