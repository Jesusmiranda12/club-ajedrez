﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Entidades;
using System.Threading.Tasks;

namespace AccesoaDatos
{
    public class SalasAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Salas salas)
        {
            if (salas.IdSala == 0)
            {
                string consulta = string.Format("call  p_insertarSalas(null,'{0}','{1}','{2}','{3}')",
                salas.Numerosala, salas.Capacidad, salas.Fkhotel, salas.Medios);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarSalas('{0}','{1}','{2}','{3}','{4}')", salas.Numerosala, salas.Capacidad, salas.Fkhotel, salas.Medios, salas.IdSala);
                conexion.Ejecutarconsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarSalas('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Salas> GetSalas(string filtro)
        {
            var listSalas = new List<Salas>();
            var ds = new DataSet();
            string consulta = "call p_verSalas('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verSalas");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var salas = new Salas
                {
                    IdSala = Convert.ToInt32(row["Idsala"]),
                    Numerosala = Convert.ToInt32(row["numerosala"]),
                    Capacidad = Convert.ToInt32(row["capacidad"]),
                    Fkhotel = row["nombreh"].ToString(),
                    Medios = row["medios"].ToString(),
                };
                listSalas.Add(salas);
            }
            return listSalas;
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listHotel = new List<Hoteles>(); 
            var ds = new DataSet();
            string consulta = "call p_verHoteles('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verHoteles");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var hotel = new Hoteles
                {
                    IdHotel = Convert.ToInt32(row["Idhotel"]),
                    Nombreh = row["nombreh"].ToString(),
                };
                listHotel.Add(hotel);
            }
            return listHotel;
        }
    }
}
