﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using Entidades;
using System.Threading.Tasks;

namespace AccesoaDatos
{
    public class PartidasAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Partidas partidas)
        {
            if (partidas.Idpartida == 0)
            {
                string consulta = string.Format("call  p_insertarPartidas(null,'{0}','{1}','{2}','{3}','{4}')",
                partidas.JugadorBlancas, partidas.JugadorNegras, partidas.Arbitro, partidas.Sala, partidas.Fecha);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarPartidas('{0}','{1}','{2}','{3}','{4}','{5}')", partidas.JugadorBlancas, partidas.JugadorNegras, partidas.Arbitro, partidas.Sala, partidas.Fecha, partidas.Idpartida);
                conexion.Ejecutarconsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarPartidas('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Partidas> GetPartidas(string filtro)
        {
            var listPartidas = new List<Partidas>();
            var ds = new DataSet();
            string consulta = "call p_verPartidas('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verPartidas");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var partidas = new Partidas
                {
                    Idpartida = Convert.ToInt32(row["idpartida"]),
                    JugadorBlancas = row["jugadorblancas"].ToString(),
                    JugadorNegras = row["jugadornegras"].ToString(),
                    Arbitro = row["arbitro"].ToString(),
                    Sala = Convert.ToInt32(row["numerosala"]),
                    Fecha = row["fecha"].ToString()
                };
                listPartidas.Add(partidas);
            }
            return listPartidas;
        }
        public List<Jugadores> GetJugadores(string filtro)
        {
            var listJugador = new List<Jugadores>();
            var ds = new DataSet();
            string consulta = "call p_verJugadores('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verJugadores");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var jugador = new Jugadores
                {
                    Idjugador = Convert.ToInt32(row["idparticipante"]),
                    Numeroasociado = row["numero_asociado"].ToString(),
                };
                listJugador.Add(jugador);
            }
            return listJugador;
        }
        public List<Arbitros> GetArbitros(string filtro)
        {
            var listArbitros = new List<Arbitros>();
            var ds = new DataSet();
            string consulta = "call p_verArbitros('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verArbitros");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var arbitro = new Arbitros
                {
                    IdArbitro = Convert.ToInt32(row["idarbitro"]),
                    Numero_asociado = row["numero_asociado"].ToString(),
                };
                listArbitros.Add(arbitro);
            }
            return listArbitros;
        }
        public List<Salas> GetSalas(string filtro)
        {
            var listSalas = new List<Salas>();
            var ds = new DataSet();
            string consulta = "call p_verSalas('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verSalas");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var sala = new Salas
                {
                    IdSala = Convert.ToInt32(row["Idsala"]),
                    Numerosala = Convert.ToInt32(row["numerosala"]),
                };
                listSalas.Add(sala);
            }
            return listSalas;
        }
    }
}
