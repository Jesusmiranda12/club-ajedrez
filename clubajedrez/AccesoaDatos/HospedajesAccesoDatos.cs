﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoaDatos
{
   public class HospedajesAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Hospedajes hospedaje)
        {
            if (hospedaje.IdHospedaje == 0)
            {
                //codigo para insertar
                string consulta = string.Format("call  p_insertarHospedajes(null,'{0}','{1}','{2}','{3}')",
                hospedaje.NumeroAsociado,hospedaje.Nombrehotel,hospedaje.FechaEntrada,hospedaje.FechaSalida);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarHospedajes('{0}','{1}','{2}','{3}','{4}')", hospedaje.NumeroAsociado,hospedaje.Nombrehotel,hospedaje.FechaEntrada,hospedaje.FechaSalida,hospedaje.IdHospedaje);
                conexion.Ejecutarconsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarHospedaje('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Hospedajes> GetHospedajes(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listHospedajes = new List<Hospedajes>(); //la variable var es generica
            var ds = new DataSet();
            string consulta = "call p_verHospedajes('" + filtro + "')";
            //   string consulta = "Select * from v_alumnos where Nombrealumno like '%" + filtro + "%'";
            ds = conexion.Obtenerdatos(consulta, "p_verHospedajes");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var hospedajes = new Hospedajes 
                {
                    IdHospedaje=Convert.ToInt32(row["id"]),
                    NumeroAsociado = row["numero_asociado"].ToString(),
                    NombreParticipante = row["NombreParticipante"].ToString(),
                    ApellidoP = row["apellidop"].ToString(),
                    ApellidoM = row["apellidom"].ToString(),
                    Direccionparticipante = row["direccion"].ToString(),
                    Telefonoparticipante = row["telefono"].ToString(),
                    Nombrehotel=row["nombreh"].ToString(),
                    Direccionhotel=row["Direccion de hotel"].ToString(),
                    Telefonohotel=row["Telefono del hotel"].ToString(),
                    FechaEntrada=row["fechaentrada"].ToString(),
                    FechaSalida=row["fechasalida"].ToString()
                };
                listHospedajes.Add(hospedajes);
            }
            //HardCodear
            //lenar lista
            return listHospedajes;
        }
        public List<Participantes> GetParticipantes(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listParticipantes = new List<Participantes>(); //la variable var es generica
            var ds = new DataSet();
            string consulta = "call p_verParticipantesconpais('" + filtro + "')";
            //   string consulta = "Select * from v_alumnos where Nombrealumno like '%" + filtro + "%'";
            ds = conexion.Obtenerdatos(consulta, "p_verParticipantesconpais");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var participante = new Participantes
                {
                    IdParticipante = Convert.ToInt32(row["idparticipante"]),
                    NumeroAsociado = Convert.ToInt32(row["numero_asociado"]),
                };
                listParticipantes.Add(participante);
            }
            return listParticipantes;
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listHoteles = new List<Hoteles>();
            var ds = new DataSet();
            string consulta = "call p_verHoteles('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verHoteles");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var hoteles = new Hoteles
                {
                    IdHotel = Convert.ToInt32(row["idhotel"]),
                    Nombreh = row["nombreh"].ToString(),
                   // Direccion = row["direccion"].ToString(),
                  //  Telefono = row["telefono"].ToString(),
                };
                listHoteles.Add(hoteles);
            }
            return listHoteles;
        }
    }
}
