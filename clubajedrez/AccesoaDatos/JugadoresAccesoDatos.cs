﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccesoaDatos
{
    public class JugadoresAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Jugadores jugadores)
        {
            string consulta = string.Format("call  p_insertarJugadores('{0}','{1}')",
            jugadores.Idjugador, jugadores.Nivel);
            conexion.Ejecutarconsulta(consulta);
            var ds = new DataSet();
            ds = conexion.Obtenerdatos(consulta, "p_insertarArbitros");
            var dt = new DataTable();
            dt = ds.Tables[0];
            string variable = "";
            foreach (DataRow row in dt.Rows)
            {
                variable = row["mensaje"].ToString();
            }
            MessageBox.Show(variable);
        }
        public void Actualizar(Jugadores jugadores)
        {
            string consulta = string.Format("call  p_actualizarJugadores('{0}','{1}')",
            jugadores.Nivel, jugadores.Idjugador);
            conexion.Ejecutarconsulta(consulta);
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarJugador('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Jugadores> GetJugadores(string filtro)
        {
            var listJugadores = new List<Jugadores>();
            var ds = new DataSet();
            string consulta = "call p_verJugadores('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verJugadores");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var jugadores = new Jugadores
                {
                    Idjugador = Convert.ToInt32(row["idparticipante"]),
                    Numeroasociado = row["numero_asociado"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellidop = row["apellidop"].ToString(),
                    Apellidom = row["apellidom"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Pais = row["Pais que representa"].ToString(),
                    Nivel = Convert.ToInt32(row["nivel"]),
                    Foto = row["foto"].ToString()
                };
                listJugadores.Add(jugadores);
            }
            return listJugadores;
        }
        public List<Participantes> GetParticipantes(string filtro)
        {
            var listParticipantes = new List<Participantes>();
            var ds = new DataSet();
            string consulta = "call p_verParticipantesconpais('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verParticipantesconpais");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var participantes= new Participantes
                {
                    IdParticipante = Convert.ToInt32(row["idparticipante"]),
                    NumeroAsociado = Convert.ToInt32(row["numero_asociado"])
                };
                listParticipantes.Add(participantes);
            }
            return listParticipantes;
        }
    }
}
