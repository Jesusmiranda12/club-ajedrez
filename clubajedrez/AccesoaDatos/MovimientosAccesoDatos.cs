﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoaDatos
{
   public  class MovimientosAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Movimientos movimientos)
        {
            if (movimientos.IdMovimiento == 0)
            {
                string consulta = string.Format("call  p_insertarMovimientos(null,'{0}','{1}','{2}','{3}')",
                movimientos.NumeroMovimiento,movimientos.Posiciones,movimientos.Comentario,movimientos.Partida );
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarMovimientos('{0}','{1}','{2}','{3}','{4}')", movimientos.NumeroMovimiento, movimientos.Posiciones, movimientos.Comentario, movimientos.Partida,movimientos.IdMovimiento);
                conexion.Ejecutarconsulta(consulta);
            }
        }

        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarMovimientos('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Movimientos> GetMivimientos(string filtro)
        {
            var listMovimientos = new List<Movimientos>();
            var ds = new DataSet();
            string consulta = "call p_verMovimientos('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verMovimientos");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var movimientos = new Movimientos
                {
                   IdMovimiento=Convert.ToInt32(row["idmovimiento"]),
                   NumeroMovimiento=Convert.ToInt32(row["numerodemovimiento"]),
                   Posiciones=row["posiciones"].ToString(),
                   Comentario=row["comentario"].ToString(),
                   Partida=row["Partida"].ToString()
                };
                listMovimientos.Add(movimientos);

            }
            return listMovimientos;
        }
        public List<Partidas>GetPartidas()
        {
            var listPartidas = new List<Partidas>();
            var ds = new DataSet();
            string consulta = "call p_verIdPartida()";
            ds = conexion.Obtenerdatos(consulta, "p_verIdPartida");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var partidas = new Partidas
                {
                    Idpartida=Convert.ToInt32(row["idpartida"])
                };

                listPartidas.Add(partidas);
            }
            return listPartidas;
        }

    }
}
