﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
using System.Windows.Forms;

namespace AccesoaDatos
{
    public class ArbitrosAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);

        public void Guardar(Arbitros arbitros)
        {           
            string consulta = string.Format("call  p_insertarArbitros('{0}')",arbitros.IdArbitro);
            conexion.Ejecutarconsulta(consulta);
            var ds = new DataSet();
            ds = conexion.Obtenerdatos(consulta, "p_insertarArbitros");
            var dt = new DataTable();
            dt = ds.Tables[0];
            string variable ="";
            foreach (DataRow row in dt.Rows)
            {
                variable = row["mensaje"].ToString();
            }
            MessageBox.Show(variable);
        }
        public void eliminar(int id)
        {
            string consulta = string.Format("p_eliminarArbitro('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Arbitros> GetArbitros(string filtro)
        {
            var listArbitros = new List<Arbitros>();
            var ds = new DataSet();
            string consulta = "call p_verArbitros('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verArbitros");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Arbitros = new Arbitros
                {
                    IdArbitro = Convert.ToInt32(row["idarbitro"]),
                    Numero_asociado = row["numero_asociado"].ToString(),
                    NombreArbitro = row["nombre"].ToString(),
                    ApelldoP = row["apellidop"].ToString(),
                    ApellidoM = row["apellidom"].ToString(),
                    Direccion = row["direccion"].ToString(),                    
                    Telefono = row["telefono"].ToString(),
                    Foto = row["foto"].ToString()
                };
                listArbitros.Add(Arbitros);
            }
            return listArbitros;
        }
    }
}
