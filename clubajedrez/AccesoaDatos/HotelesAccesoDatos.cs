﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace AccesoaDatos
{
    public class HotelesAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Hoteles hoteles)
        {
            if (hoteles.IdHotel == 0)
            {
                string consulta = string.Format("call  p_insertarHoteles(null,'{0}','{1}','{2}')",
                hoteles.Nombreh, hoteles.Direccion, hoteles.Telefono);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarHoteles('{0}','{1}','{2}','{3}')", hoteles.Nombreh, hoteles.Direccion, hoteles.Telefono, hoteles.IdHotel);
                conexion.Ejecutarconsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarHoteles('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Hoteles> GetHoteles(string filtro)
        {
            var listHoteles = new List<Hoteles>(); 
            var ds = new DataSet();
            string consulta = "call p_verHoteles('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verHoteles");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var hoteles = new Hoteles
                {
                    IdHotel = Convert.ToInt32(row["idhotel"]),
                    Nombreh = row["nombreh"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                };
                listHoteles.Add(hoteles);

            }
            return listHoteles;
        }
    }
}
