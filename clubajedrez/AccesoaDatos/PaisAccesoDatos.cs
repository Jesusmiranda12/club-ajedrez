﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace AccesoaDatos
{
    public class PaisAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public void Guardar(Pais pais)
        {
            if (pais.Id_Pais == 0)
            {
                //codigo para insertar
                string consulta = string.Format("call  p_insertarPaises(null,'{0}','{1}','{2}')",
                pais.Nombre_de_pais,pais.Numero_de_clubs,pais.Pais_que_representa);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                //update o que lo modifique
                string consulta = string.Format("call p_actualizarPaises('{0}','{1}','{2}','{3}')", pais.Nombre_de_pais,pais.Numero_de_clubs,pais.Pais_que_representa,pais.Id_Pais);
                conexion.Ejecutarconsulta(consulta);
            }
        }
        public void Eliminar(int id)
        {
            string consulta = string.Format("p_eliminarPaises('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Pais> GetPaises(string filtro)
        {
            var listPais = new List<Pais>(); //la variable var es generica
            var ds = new DataSet();
            string consulta = "call p_verPaises('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verPaises");       
            var dt = new DataTable();
            dt = ds.Tables[0]; 
            foreach (DataRow row in dt.Rows)
            {
                var pais = new Pais();
                pais.Id_Pais = Convert.ToInt32(row["idpais"]);
                pais.Nombre_de_pais = row["nombre"].ToString();       
                pais.Numero_de_clubs = Convert.ToInt32(row["numeroclubs"] is DBNull ? 0 : row["numeroclubs"]);
                pais.Pais_que_representa = Convert.ToString(row["PaisRepresenta"] is DBNull ? 0 : row["PaisRepresenta"]);
                listPais.Add(pais);
            }
            return listPais;
        }
        public List<Pais> GetPaisesRepresenta(string filtro)
        {
            var listPais = new List<Pais>(); //la variable var es generica
            var ds = new DataSet();
            string consulta = "call p_verNombrePais('" + filtro + "')";
            ds = conexion.Obtenerdatos(consulta, "p_verNombrePais");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var pais = new Pais
                {
                    Id_Pais = Convert.ToInt32(row["idpais"]),
                    Nombre_de_pais = row["nombre"].ToString(),
                };
                listPais.Add(pais);

            }
            return listPais;
        }
    }
}
